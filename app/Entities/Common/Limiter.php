<?php

namespace App\Entities\Common;

class Limiter
{
    public int $limit;
    public int|null $offset = null;
}

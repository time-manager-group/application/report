<?php

namespace App\Entities;

class KVMetric
{
    /**
     * @var string[]
     */
    public array $key;
    public int $createdAt;
    public int|null $updatedAt = null;
    public int|null $ttl = null;

    public array|string|int|float|null $value;
}

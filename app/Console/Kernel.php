<?php

namespace App\Console;

use App\Modules\Report\Services\ReportService;
use App\Services\MetricService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->call(function (ReportService $reportService, MetricService $metricService): void {
            $metricService->setNowTime('cron:sendDailyReport:before');
            $reportService->sendDailyReport();
            $metricService->setNowTime('cron:sendDailyReport:after');
        })->at('12:25');

        $schedule->call(function (MetricService $metricService): void {
            $metricService->setNowTime('cron:everyMinute');
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}

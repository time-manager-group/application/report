<?php

namespace App\Repositories;

use App\Entities\KVMetric;
use App\Tools\EntityHelper;
use Illuminate\Cache\Repository;

class MetricKVRepository
{
    private Repository $cache;

    public function __construct()
    {
        $this->cache = app('cache')->store('file');
    }


    public function set(KVMetric $metric): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->cache->set(self::strKey($metric->key), [
            'createdAt' => $metric->createdAt,
            'updatedAt' => $metric->updatedAt,
            'ttl' => $metric->ttl,
            'value' => $metric->value,
        ], $metric->ttl);
    }

    public function unset(array $key): void
    {
        $this->cache->forget(self::strKey($key));
    }

    public function has(array $key): bool
    {
        return $this->cache->has(self::strKey($key));
    }

    public function get(array $key): KVMetric|null
    {
        if (!$this->cache->has(self::strKey($key))) {
            return null;
        }
        $arr = $this->cache->get(self::strKey($key));

        return EntityHelper::fromArr($arr + ['key' => $key], KVMetric::class);
    }

    private static function strKey(array $key): string
    {
        return json_encode($key);
    }
}

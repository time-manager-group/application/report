<?php

use App\Tools\EntityHelper;

if (!function_exists('fromArr')) {
    /**
     * @template T
     * @param array             $array
     * @param T|class-string<T> $classNameOrObject
     * @param bool              $filterNull
     * @return T
     * @noinspection PhpDocSignatureInspection
     */
    function fromArr(array $array, object|string $classNameOrObject, bool $filterNull = false): object
    {
        return EntityHelper::fromArr($array, $classNameOrObject, $filterNull);
    }
}

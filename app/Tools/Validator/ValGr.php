<?php

namespace App\Tools\Validator;

class ValGr
{
    /**
     * @param             $value
     * @param Validator[] $validators
     */
    public function __construct(public $value, public array $validators)
    {
    }

    /**
     * @param                       $value
     * @param Validator|Validator[] $validators
     * @return self
     */
    public static function m($value, Validator|array $validators): self
    {
        return new self($value, is_array($validators) ? $validators : [$validators]);
    }

    public function nullable(bool $nullable = true): self
    {
        foreach ($this->validators as $validator) {
            $validator->nullable($nullable);
        }
        return $this;
    }


    public function mustEmpty(bool $mustEmpty = true): self
    {
        foreach ($this->validators as $validator) {
            $validator->mustEmpty($mustEmpty);
        }
        return $this;
    }
}

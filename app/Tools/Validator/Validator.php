<?php

namespace App\Tools\Validator;

interface Validator
{
    public function validate($value): Err|null;
}

<?php

namespace App\Tools\Validator;

use App\Exceptions\ValidateException;

class ValidateManager
{
    /**
     * @var  array<string,string|array>
     */
    private array $errors = [];

    /**
     * @param string               $attribute
     * @param array<string, ValGr> $array
     * @throws ValidateException
     */
    public static function validateOrAddErrorsAndThrow(string $attribute, array $array): void
    {
        self::validateOrAddErrors(new self(), $attribute, $array)->tryThrow();
    }

    /**
     * @param array<string, ValGr> $array
     */
    public static function validateOrAddErrors(self $validatorManager, string $attribute, array $array): self
    {
        $currentValidator = (new self())->validate($array);
        if ($currentValidator->hasErrors()) {
            $validatorManager->addError($attribute, $currentValidator->getErrors());
        }

        return $validatorManager;
    }

    /**
     * @param array<string, ValGr> $array
     * @throws ValidateException
     */
    public static function validateOrThrow(array $array): void
    {
        (new self())->validate($array)->tryThrow();
    }

    public static function m(): self
    {
        return new self();
    }

    /**
     * @param string       $attribute
     * @param string|array $error
     * @return void
     */
    public function addError(string $attribute, string|array $error): void
    {
        if (!array_key_exists($attribute, $this->errors)) {
            $this->errors[$attribute] = [];
        }

        if (is_array($error)) {
            foreach ($error as $field => $errorItem) {
                $this->errors[$attribute][$field] = $errorItem;
            }
            return;
        }

        $this->errors[$attribute][] = $error;
    }

    public function hasErrors(): bool
    {
        return count($this->errors) > 0;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array<string, ValGr> $array
     */
    public function validate(array $array): self
    {
        foreach ($array as $attribute => $valGr) {
            foreach ($valGr->validators as $validator) {
                $err = $validator->validate($valGr->value);
                if (is_null($err)) {
                    continue;
                }
                $this->addError($attribute, $err->message);
                switch ($err->howNext) {
                    case Err::N_DEFAULT:
                        break;
                    case Err::N_STOP_ATTRIBUTE:
                        break 2;
                    case Err::N_STOP_ALL:
                        break 3;
                }
            }
        }

        return $this;
    }

    /**
     * @throws ValidateException
     */
    public function tryThrow(): void
    {
        if ($this->hasErrors()) {
            throw new ValidateException($this->getErrors());
        }
    }
}

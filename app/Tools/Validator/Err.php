<?php

namespace App\Tools\Validator;

class Err
{

    public const N_DEFAULT = 'default';
    public const N_STOP_ATTRIBUTE = 'stop_attribute';
    public const N_STOP_ALL = 'stop_all';

    public function __construct(public array|string $message, public string $howNext = 'default')
    {
    }

    /**
     * @param string $message
     * @param string $howNext
     * @return self
     */
    public static function m(array|string $message, string $howNext = 'default'): self
    {
        return new self($message, $howNext);
    }
}

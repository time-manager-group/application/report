<?php

namespace App\Tools\Validator\Validators;

class Str extends BaseValidator
{
    public const MAX_ASCII = 1;
    public const MAX_BMP = 3;
    public const MAX_4BYTES = 4;

    private int $min = 0;
    private int $max = 255;
    private int $maxBytes = self::MAX_BMP;

    public function min(int $min): self
    {
        $this->min = $min;
        return $this;
    }

    public function max(int $max): self
    {
        $this->max = $max;
        return $this;
    }

    public function length(int $min = 0, int $max = 255): self
    {
        $this->min = $min;
        $this->max = $max;
        return $this;
    }

    public function maxBytes(int $maxBytes = self::MAX_BMP): self
    {
        $this->maxBytes = $maxBytes;
        return $this;
    }


    public function validateValue($value): string|null
    {
        if (!is_string($value)) {
            return 'not string';
        }
        if (mb_strlen($value) < $this->min) {
            return "minimum length must be $this->min characters";
        }
        if (mb_strlen($value) > $this->max) {
            return "maximum length must be $this->max characters";
        }
        $badCharacter = $this->findBadCharacter($value);
        if (!is_null($badCharacter)) {
            return "bad character `$badCharacter`";
        }
        return null;
    }

    /**
     * Используем тот факт что strlen считает строку побайтно, а mb_strlen - посимвольно для определения количество занимаемых символом байт
     * @param $value
     * @return Str|null
     */
    private function findBadCharacter($value): string|null
    {
        for ($i = 0; $i < mb_strlen($value); $i++) {
            $char = mb_substr($value, $i, 1);
            if (strlen($char) > $this->maxBytes) {
                return $char;
            }
        }
        return null;
    }
}

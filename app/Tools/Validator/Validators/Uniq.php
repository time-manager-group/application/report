<?php

namespace App\Tools\Validator\Validators;

class Uniq extends BaseValidator
{
    private array $enums = [];
    private array $excluded = [];

    public function enums(array $enums): self
    {
        $this->enums = $enums;
        return $this;
    }

    public function excluded(string|array $excluded): self
    {
        $this->excluded = is_string($excluded) ? [$excluded] : $excluded;
        return $this;
    }

    public function validateValue($value): string|null
    {
        if (in_array($value, $this->enums, true) && !in_array($value, $this->excluded, true)) {
            return 'not unique';
        }
        return null;
    }

}

<?php

namespace App\Tools\Validator\Validators;

use App\Tools\Validator\Err;
use App\Tools\Validator\Validator;

abstract class BaseValidator implements Validator
{
    private bool $nullable = false;
    private bool $mustEmpty = false;
    private string $howNext = Err::N_DEFAULT;

    public static function m(string $howNext = Err::N_STOP_ATTRIBUTE): static
    {
        $instance = new static();
        $instance->howNext = $howNext;
        return $instance;
    }

    public function nullable(bool $nullable = true): static
    {
        $this->nullable = $nullable;
        return $this;
    }

    public function mustEmpty(bool $mustEmpty = true): static
    {
        $this->mustEmpty = $mustEmpty;
        return $this;
    }

    public function validate($value): Err|null
    {
        if ($this->nullable && is_null($value)) {
            return null;
        }
        if ($this->mustEmpty && empty($value)) {
            return null;
        }

        $errText = static::validateValue($value);
        if (is_null($errText)) {
            return null;
        }
        return Err::m($errText, $this->howNext);
    }

    abstract protected function validateValue($value): array|string|null;
}

<?php

namespace App\Tools\Validator\Validators;

class Req extends BaseValidator
{
    public function validateValue($value): string|null
    {
        if (empty($value)) {
            return 'required';
        }
        return null;
    }
}

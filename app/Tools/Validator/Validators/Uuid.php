<?php

namespace App\Tools\Validator\Validators;

class Uuid extends BaseValidator
{
    public function validateValue($value): string|null
    {
        $err = Str::m()->length(36, 36)->maxBytes(Str::MAX_ASCII)->validate($value);
        if (!is_null($err)) {
            return $err->message;
        }
        if (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $value) !== 1) {
            return 'bad format';
        }
        return null;
    }
}

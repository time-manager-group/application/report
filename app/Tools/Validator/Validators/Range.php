<?php

namespace App\Tools\Validator\Validators;

class Range extends BaseValidator
{
    private array $enums = [];

    public function enums(array $enums): self
    {
        $this->enums = $enums;
        return $this;
    }

    public function validateValue($value): string|null
    {
        if (!in_array($value, $this->enums, true)) {
            return 'not exists';
        }
        return null;
    }

}

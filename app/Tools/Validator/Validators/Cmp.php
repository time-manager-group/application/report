<?php

namespace App\Tools\Validator\Validators;

class Cmp extends BaseValidator
{
    private mixed $comparableValue;

    private bool $same = true;

    public function comparable($value): self
    {
        $this->comparableValue = $value;
        return $this;
    }

    public function same($same = true): self
    {
        $this->same = $same;
        return $this;
    }

    protected function validateValue($value): string|null
    {
        if ($this->same && $this->comparableValue !== $value) {
            return 'is different';
        }
        if (!$this->same && $this->comparableValue == $value) {
            return 'is the same';
        }
        return null;
    }
}

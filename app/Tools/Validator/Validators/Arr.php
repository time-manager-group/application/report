<?php

namespace App\Tools\Validator\Validators;

use App\Tools\Validator\ValGr;
use App\Tools\Validator\ValidateManager;

class Arr extends BaseValidator
{
    private int $min = 0;
    private int|null $max = null;
    private bool $unique = true;
    private bool $indexed = true;

    private ValGr|null $each = null;

    public function min(int $min): self
    {
        $this->min = $min;
        return $this;
    }

    public function max(int $max): self
    {
        $this->max = $max;
        return $this;
    }

    public function unique(bool $unique = true): self
    {
        $this->unique = $unique;
        return $this;
    }

    public function indexed(bool $indexed = true): self
    {
        $this->indexed = $indexed;
        return $this;
    }

    public function each(ValGr|BaseValidator $each): self
    {
        if ($each instanceof BaseValidator) {
            $each = ValGr::m(null, $each);
        }
        $this->each = $each;
        return $this;
    }

    protected function validateValue($value): array|string|null
    {
        if (!is_array($value)) {
            return 'not an array';
        }
        if (count($value) < $this->min) {
            return "minimum count must be $this->min elements";
        }
        if (!is_null($this->max) && count($value) > $this->max) {
            return "maximum count must be $this->max elements";
        }

        if ($this->indexed) {
            foreach (array_keys($value) as $key => $item) {
                if ((string)$key !== (string)$item) {
                    return 'array is not indexed';
                }
            }
        }

        if (!is_null($this->each)) {
            $validators = [];
            $manager = new ValidateManager();
            foreach ($value as $key => $item) {
                $valGr = clone $this->each;
                $valGr->value = $item;
                $validators[$key] = $valGr;
            }
            if ($manager->validate($validators)->hasErrors()) {
                return $manager->getErrors();
            }
        }

        if ($this->unique && count(array_unique($value)) !== count($value)) {
            return 'values must be unique';
        }

        return null;
    }
}

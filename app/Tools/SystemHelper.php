<?php

namespace App\Tools;

use Illuminate\Support\Str;

class SystemHelper
{
    private static string|null $requestId = null;

    public static function getRequestId(): string
    {
        if (self::$requestId === null) {
            self::$requestId = Str::uuid()->toString();
        }
        return self::$requestId;
    }
}

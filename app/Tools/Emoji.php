<?php

namespace App\Tools;

class Emoji
{
    const BAR_CHART = "📊";
    const NOTEBOOK = "🗒";
    const GREEN_CHECK = "✅";
    const GREY_CHECK = "☑️";
    const GREEN_CIRCLE = "🟢";
    const WHITE_CIRCLE = "⚪️";
    const BLUE_CIRCLE = "🔵";
    const BOARD = "🪧";
    const PUSH_PIN = "📌";
    const UL_1 = "🔸";
    const UL_2 = "🔻";
    const UL_3 = "🔹";
    const UL_4 = "▫️";
    const UL_5 = "🔺";
    const UL_ICONS = [self::UL_1, self::UL_2, self::UL_3, self::UL_4, self::UL_5];
}

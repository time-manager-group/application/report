<?php
/** @noinspection PhpDocMissingThrowsInspection */

namespace App\Tools;

class EntityHelper
{
    /**
     * @template T
     * @param array             $array
     * @param T|class-string<T> $classNameOrObject
     * @param bool              $filterNull
     * @return T
     * @noinspection PhpDocSignatureInspection
     */
    public static function fromArr(array $array, object|string $classNameOrObject, bool $filterNull = false): object
    {
        if (is_string($classNameOrObject)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $entity = app()->make($classNameOrObject);
        } else {
            $entity = $classNameOrObject;
        }
        if ($filterNull) {
            $array = array_filter($array, function ($value) {
                return $value !== null;
            });
        }
        foreach ($array as $property => $value) {
            $entity->{$property} = $value;
        }

        return $entity;
    }
}

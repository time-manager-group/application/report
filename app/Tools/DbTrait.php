<?php

namespace App\Tools;

use App\Entities\Common\Limiter;
use App\Modules\Note\Services\DTO\StorageStatInfo;
use Closure;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use stdClass;

trait DbTrait
{
    public function transaction(Closure $closure): void
    {
        DB::transaction($closure);
    }

    public function startTransaction(): void
    {
        DB::beginTransaction();
    }

    public function commitTransaction(): void
    {
        DB::commit();
    }

    public function rollBackTransaction(): void
    {
        DB::rollBack();
    }

    public static function getTableSize(string $tableName): int
    {
        return (int)DB::select("SELECT data_length + index_length `size`
FROM information_schema.TABLES
WHERE table_schema = DATABASE() AND table_name = ?;
            ", [$tableName])[0]->size ?? 0;
    }

    public static function getTableRowStat(string $tableName): stdClass
    {
        return DB::table($tableName)->selectRaw('min(time) as min_time, max(time) as max_time, count(*) as count')->get()->first();
    }

    public static function getTableStat(string $tableName): StorageStatInfo
    {
        $rowStat = self::getTableRowStat($tableName);
        return fromArr([
            'sizeByte' => self::getTableSize($tableName),
            'rowsCount' => $rowStat->count ?? 0,
            'rowOdlTime' => $rowStat->min_time ? TimeHelper::dbFormatToTs($rowStat->min_time) : 0,
            'rowLastTime' => $rowStat->max_time ? TimeHelper::dbFormatToTs($rowStat->max_time) : 0,
        ], StorageStatInfo::class);
    }

    public function setLimit(Builder $query, Limiter $limiter): Builder
    {
        $query->limit($limiter->limit);
        if (!is_null($limiter->offset)) {
            $query->offset($limiter->offset);
        }
        return $query;
    }
}

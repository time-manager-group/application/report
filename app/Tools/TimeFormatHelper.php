<?php

namespace App\Tools;

use Carbon\Carbon;

class TimeFormatHelper
{
    const RELATIVE_TENSE_IN_WORDS_DICT = [
        -7 => 'a week ago',
        -6 => '6 days ago',
        -5 => '5 days ago',
        -4 => '4 days ago',
        -3 => '3 days ago',
        -2 => 'the day before yesterday',
        -1 => 'yesterday',
        0 => 'today',
        1 => 'tomorrow',
        +2 => 'day after tomorrow',
    ];

    const RELATIVE_TENSE_IN_WORDS_DICT_RU = [
        -7 => 'неделю назад',
        -6 => '6 дней назад',
        -5 => '5 дней назад',
        -4 => '4 дня назад',
        -3 => '3 дня назад',
        -2 => 'позавчера',
        -1 => 'вчера',
        0 => 'сегодня',
        1 => 'завтра',
        +2 => 'послезавтра',
    ];

    const DAY_OF_WEEK_DICT = [
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
        7 => 'sunday',
    ];

    const DAY_OF_WEEK_DICT_RU = [
        1 => 'понедельник',
        2 => 'вторник',
        3 => 'среда',
        4 => 'четверг',
        5 => 'пятница',
        6 => 'суббота',
        7 => 'воскресенье',
    ];


    const TIME_FORMAT_INTERVAL_DICT = [
        's' => 's',
        'second_1' => 'second',
        'second_2' => 'second',
        'second_3' => 'second',
        'm' => 'm',
        'minute_1' => 'minute',
        'minute_2' => 'minute',
        'minute_3' => 'minute',
        'h' => 'h',
        'hour_1' => 'hour',
        'hour_2' => 'hour',
        'hour_3' => 'hour',
    ];

    const TIME_FORMAT_INTERVAL_DICT_RU = [
        's' => 'с',
        'second_1' => 'секунда',
        'second_2' => 'секунды',
        'second_3' => 'секунд',
        'm' => 'м',
        'minute_1' => 'минута',
        'minute_2' => 'минуты',
        'minute_3' => 'минут',
        'h' => 'ч',
        'hour_1' => 'час',
        'hour_2' => 'часа',
        'hour_3' => 'часов',
    ];

    public static function getDayOfWeak(int $currentTime, array $dictionary = self::DAY_OF_WEEK_DICT): string
    {
        $dayOfWeek = Carbon::createFromTimestamp($currentTime)->dayOfWeek;
        if ($dayOfWeek === 0) {
            $dayOfWeek = 7;
        }
        return $dictionary[$dayOfWeek];
    }

    public static function getRelativeTenseInWords(int $comparableTime, array $dictionary = self::RELATIVE_TENSE_IN_WORDS_DICT): string|null
    {
        $comparableTime = TimeHelper::startDayTs($comparableTime);
        $todayTime = TimeHelper::startDayTs(Carbon::now()->getTimestamp());

        $multiplier = $todayTime > $comparableTime ? -1 : 1;

        $daysCount = (int)round(abs($todayTime - $comparableTime) / 3600 / 24);

        return $dictionary[$daysCount * $multiplier] ?? null;
    }

    public static function getFormatedInterval(int $seconds, $short = true, array $dictionary = self::TIME_FORMAT_INTERVAL_DICT): string
    {
        return $short ? self::formatIntervalShort($seconds, $dictionary) : self::formatInterval($seconds, $dictionary);
    }

    private static function formatInterval(int $diffSeconds, array $dictionary): string
    {
        $hours = intval($diffSeconds / 3600);
        $minutes = intval(($diffSeconds % 3600) / 60);
        $seconds = ($diffSeconds % 3600) % 60;

        $format = '';

        if ($diffSeconds === 0) {
            $format = self::getSeconds(0, $dictionary);
        } else {
            if ($hours) {
                $format .= self::getHours($hours, $dictionary) . ' ';
            }
            if ($minutes) {
                $format .= self::getMinutes($minutes, $dictionary) . ' ';
            }
            if ($seconds) {
                $format .= self::getSeconds($seconds, $dictionary) . ' ';
            }
        }

        return trim(preg_replace('/\s+/', ' ', $format));
    }

    private static function formatIntervalShort(int $diffSeconds, array $dictionary): string
    {
        $hours = intval($diffSeconds / 3600);
        $minutes = intval(($diffSeconds % 3600) / 60);
        $seconds = ($diffSeconds % 3600) % 60;

        $format = '';

        if ($diffSeconds === 0) {
            $format = self::getSeconds(0, $dictionary);
        } else {
            if ($hours) {
                $format .= $hours . "{$dictionary['h']} ";
            }
            if ($minutes) {
                $format .= $minutes . "{$dictionary['m']} ";
            }
            if ($seconds) {
                $format .= $seconds . "{$dictionary['s']} ";
            }
        }

        return trim(preg_replace('/\s+/', ' ', $format));
    }

    private static function getSeconds($seconds, array $dictionary): string
    {
        return self::getDescTime($seconds, $dictionary['second_1'], $dictionary['second_2'], $dictionary['second_3']);
    }

    private static function getMinutes($minutes, array $dictionary): string
    {
        return self::getDescTime($minutes, $dictionary['minute_1'], $dictionary['minute_2'], $dictionary['minute_3']);
    }

    private static function getHours($hours, array $dictionary): string
    {
        return self::getDescTime($hours, $dictionary['hour_1'], $dictionary['hour_2'], $dictionary['hour_3']);
    }

    private static function getDescTime($value, $text1, $text2, $text3): string
    {
        $valueArray = array_reverse(str_split((string)$value));
        $oneNumber = isset($valueArray[0]) ? intval($valueArray[0]) : 0;
        $twoNumber = isset($valueArray[1]) ? intval($valueArray[1]) : 0;

        if ($twoNumber === 0 || $twoNumber > 1) {
            if ($oneNumber === 0 || $oneNumber > 4) {
                return $value . ' ' . $text3;
            }

            if ($oneNumber === 1) {
                return $value . ' ' . $text1;
            }

            return $value . ' ' . $text2;
        }

        return $value . ' ' . $text3;
    }

}

<?php

namespace App\Tools;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

/**
 * Class Serializer.
 * Сериалайзер.
 *
 * @package App\Tools
 */
class Serializer
{
    private SymfonySerializer $serializer;

    /**
     * Serializer constructor.
     *
     * @param ObjectNormalizer $objectNormalizer
     * @param JsonEncoder      $jsonEncoder
     */
    public function __construct(ObjectNormalizer $objectNormalizer, JsonEncoder $jsonEncoder)
    {
        $this->serializer = new SymfonySerializer(
            [$objectNormalizer],
            ['json' => $jsonEncoder]
        );
    }

    /**
     * Трансформировать объект или список объектов в json ответ.
     *
     * @param mixed $data
     * @return Response
     */
    public function serialize(mixed $data): Response
    {
        return $this->jsonContent($this->jsonSerialize($data));
    }

    /**
     * Успешный ответ c готовым json контентом.
     *
     * @param string $content
     * @return Response
     */
    private function jsonContent(string $content): Response
    {
        $response = new Response();
        $response->setContent($content);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Сериализация в json.
     *
     * @param mixed $data
     * @return string
     */
    private function jsonSerialize(mixed $data): string
    {
        return $this->serializer->serialize($data, 'json');
    }
}

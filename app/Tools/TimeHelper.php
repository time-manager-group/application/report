<?php

namespace App\Tools;

use Carbon\Carbon;

class TimeHelper
{
    public const SECOND = 1;
    public const MINUTE = 60 * self::SECOND;
    public const HOUR = 60 * self::MINUTE;
    public const DAY = 24 * self::HOUR;
    public const FORMAT_DB = 'Y-m-d H:i:s';

    public static function dbFormatToTs(string|null $formatTime): int|null
    {
        return $formatTime ? (int)Carbon::createFromFormat(self::FORMAT_DB, $formatTime)->timestamp : null;
    }

    public static function tsToDbFormat(int|null $timestamp): string|null
    {
        return $timestamp ? Carbon::createFromTimestamp($timestamp)->format(self::FORMAT_DB) : null;
    }

    public static function startDayTs(int $currentTime): int
    {
        return Carbon::createFromTimestamp($currentTime)->startOfDay()->getTimestamp();
    }

    public static function nextDayTs(int $currentTime): int
    {
        return Carbon::createFromTimestamp($currentTime)->endOfDay()->getTimestamp();
    }
}

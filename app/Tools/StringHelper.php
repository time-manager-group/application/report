<?php

namespace App\Tools;

class StringHelper
{
    public static function addBeforeRow(string $str, string $before, $separator = "\n"): string
    {
        return implode($separator, array_map(fn($row) => "$before$row", explode($separator, $str)));
    }
}

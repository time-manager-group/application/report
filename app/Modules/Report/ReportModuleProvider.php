<?php

namespace App\Modules\Report;

use App\Modules\Report\Components\TgBotClient;
use App\Modules\Report\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ReportModuleProvider extends ServiceProvider
{
    public function boot(): void
    {
        Route::middleware(['web'])->group(function () {
            Route::get('/get-daily-track-report', [ReportController::class, 'getDailyTrackReport']);
            Route::post('/send-daily-report', [ReportController::class, 'sendDailyReport']);
        });
        app()->bind(TgBotClient::class, function () {
            return new TgBotClient($this->app['config']['services.tgbot.token']);
        });
    }
}

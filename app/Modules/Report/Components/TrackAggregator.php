<?php

namespace App\Modules\Report\Components;

use App\Modules\Tracker\Entities\Track;
use RuntimeException;

class TrackAggregator
{
    /**
     * @param Track[] $tracks
     */
    public function __construct(private array $tracks)
    {

    }

    /**
     * @param Track::T_USELESS|Track::T_UNDEFINED|Track::T_USEFUL $type
     * @return array
     */
    public function makeTree($type): array
    {
        $tasks = self::getTasks($this->getTracksByType($type));
        return self::buildTree($tasks);
    }

    /**
     * @param Track::T_USELESS|Track::T_UNDEFINED|Track::T_USEFUL $type
     * @return Track[]
     */
    private function getTracksByType($type): array
    {
        return array_filter($this->tracks, function (Track $track) use ($type): bool {
            return $track->type === $type;
        });
    }

    /**
     * @param Track[] $tracks
     */
    private static function getTasks(array $tracks): array
    {
        $tasks = [];
        foreach ($tracks as $track) {
            if (!($tasks[$track->getPreparedName()] ?? false)) {
                $tasks[$track->getPreparedName()] = [
                    'preparedName' => $track->getPreparedName(),
                    'name' => $track->name,
                    'allSeconds' => 0,
                    'tokens' => $track->getTokens(),
                ];
            }
            $tasks[$track->getPreparedName()]['allSeconds'] += $track->getSeconds();
        }

        return $tasks;
    }

    private static function buildTree(array $tasks): array
    {

        $tree = [];
        foreach ($tasks as $task) {
            $currentLevel = &$tree;
            $currentTokens = [];
            foreach ($task['tokens'] as $i => $token) {
                $currentTokens[] = $token;
                $isCompleted = count($task['tokens']) - 1 === $i;
                $tokenBody = [
                    'token' => $token,
                    'isCompleted' => $isCompleted,
                    'allSeconds' => $isCompleted ? $task['allSeconds'] : null,
                    'currentTokens' => $currentTokens,
                    'children' => [],
                ];

                $item = self::getItemIndexByToken($currentLevel, $token);

                if (is_null($item)) {
                    $currentLevel[] = $tokenBody;
                } else {
                    if ($tokenBody['isCompleted']) {
                        $currentLevel[$item]['isCompleted'] = $tokenBody['isCompleted'];
                        $currentLevel[$item]['allSeconds'] = $tokenBody['allSeconds'];
                    }
                }

                $currentLevel = &$currentLevel[self::getItemIndexByToken($currentLevel, $token)]['children'];
            }
        }

        self::addCommonTask($tree);
        $allSeconds = self::calcChildrenSeconds($tree);
        self::sortTree($tree);
        self::collapseTree($tree);

        return ['allSeconds' => $allSeconds, 'children' => $tree];
    }

    private static function getItemIndexByToken(array $level, string $token): int|null
    {
        foreach ($level as $index => $item) {
            if ($item['token'] === $token) {
                return $index;
            }
        }
        return null;
    }

    private static function addCommonTask(array &$tree): void
    {
        $commonTaskName = '[Основная задача]';
        foreach ($tree as &$item) {
            if (!array_key_exists('isCommonTask', $item)) {
                $item['isCommonTask'] = false;
            }
            if ($item['isCompleted'] && count($item['children']) > 0) {
                $item['children'] = [
                    [
                        'isCommonTask' => true,
                        'isCompleted' => true,
                        'token' => $commonTaskName,
                        'allSeconds' => $item['allSeconds'],
                        'currentTokens' => $item['currentTokens'] + [$commonTaskName],
                        'children' => [],
                    ],
                    ...$item['children'],
                ];
                $item['allSeconds'] = null;
                $item['isCompleted'] = false;
            }
            self::addCommonTask($item['children']);
        }
    }

    private static function calcChildrenSeconds(array &$tree): int
    {
        $allSeconds = 0;
        foreach ($tree as &$item) {
            if ($item['isCompleted'] && is_null($item['allSeconds'])) {
                throw new RuntimeException('Bad schema token: ' . $item['token']);
            }
            if ($item['isCompleted'] && count($item['children']) > 0) {
                throw new RuntimeException('Bad schema token: ' . $item['token']);
            }
            if (!$item['isCompleted']) {
                $item['allSeconds'] = self::calcChildrenSeconds($item['children']);
            }
            $allSeconds += $item['allSeconds'];
        }

        return $allSeconds;
    }

    private static function sortTree(array &$tree): void
    {
        usort($tree, function ($a, $b) {
            if ($a['allSeconds'] === $b['allSeconds']) {
                return 0;
            }
            return $a['allSeconds'] < $b['allSeconds'] ? 1 : -1;
        });

        foreach ($tree as &$item) {
            if (is_null($item['allSeconds'])) {
                throw new RuntimeException('Bad schema token: ' . $item['token']);
            }
            self::sortTree($item['children']);
        }
    }

    private static function collapseTree(array &$tree): void
    {
        foreach ($tree as $i => &$item) {
            if (count($item['children']) > 0) {
                self::collapseTree($item['children']);
            }
            if (count($item['children']) === 1) {
                $parentToken = $item['token'];
                $onceChild = [];
                foreach ($item['children'][0] as $field => $value) {
                    $onceChild[$field] = $value;
                }
                $onceChild['token'] = $parentToken . Track::TOKEN_SEPARATOR . ' ' . $onceChild['token'];
                $onceChild['isCollapsed'] = true;
                $tree[$i] = $onceChild;
                $item = $onceChild;
            } else {
                $item['isCollapsed'] = false;
            }
        }
    }
}

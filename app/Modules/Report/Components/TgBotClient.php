<?php

namespace App\Modules\Report\Components;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class TgBotClient
{
    private Client $client;

    public function __construct(string $token)
    {
        $this->client = new Client(['base_uri' => "https://api.telegram.org/bot$token/"]);
    }

    /**
     * @throws GuzzleException
     */
    public function sendMessage(int $chatId, string $message, $isMarkDown = false): void
    {
        $this->client->post('sendMessage', [
            RequestOptions::JSON => [
                'chat_id' => $chatId,
                'text' => $message,
                'parse_mode' => $isMarkDown ? 'Markdown' : 'HTML',
            ],
        ]);
    }
}

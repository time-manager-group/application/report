<?php

namespace App\Modules\Report\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Exceptions\ValidateException;
use App\Http\Controllers\Controller;
use App\Modules\Report\Services\ReportService;
use App\Modules\Report\Services\TrackReportService;
use App\Modules\Tracker\Entities\Track;
use App\Services\MetricService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ReportController extends Controller
{

    /**
     * @throws NotFoundException|ValidateException|GuzzleException|ContainerExceptionInterface|NotFoundExceptionInterface
     */
    public function sendDailyReport(ReportService $reportService, MetricService $metricService): Response
    {
        $metricService->setNowTime('http:sendDailyReport:before');
        $reportService->sendDailyReport();
        $metricService->setNowTime('http:sendDailyReport:after');
        return response('');
    }

    public function getDailyTrackReport(TrackReportService $reportService): JsonResponse
    {
        try {
            return response()->json([
                'data' => $reportService->makeDailyTreeReport([Track::T_USEFUL, Track::T_UNDEFINED]),
            ]);
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), ['types']);
        }
    }
}

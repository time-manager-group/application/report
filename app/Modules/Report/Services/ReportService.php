<?php

namespace App\Modules\Report\Services;

use App\Exceptions\NotFoundException;
use App\Exceptions\ValidateException;
use App\Modules\Note\Entities\Row;
use App\Modules\Note\Http\Controllers\NoteController;
use App\Modules\Note\Services\NoteService;
use App\Modules\Report\Components\TgBotClient;
use App\Modules\Tracker\Entities\Track;
use App\Tools\Emoji;
use App\Tools\StringHelper;
use App\Tools\TimeFormatHelper;
use App\Tools\TimeHelper;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ReportService
{
    private const NOTE_ID = NoteController::NOTE_ID;
    // Группы, которые показываются и в каком порядке
    private const NOTE_GROUP_IDS = [
        'bb1ec804-006d-42fe-b6b6-ed5776307e61',
        'ce2a9d94-2ac2-4ef7-9171-d573a27821bb',
        '066736c4-8503-4b35-b2f3-e92b218fd465',
        '35a21638-b478-46fb-80b2-382224be1846',
        '472ab5f2-399a-48b1-961c-a9672bcdc5fd',
        '5cb1dc6c-8d98-4be1-a0a1-68ac725199e2',
        '10eecb64-b39f-44db-a1ba-d075378fb6b0',
        '6a36fa70-995f-40b4-86ec-350a8ec20805',
        'b42109ca-864c-467a-9ce7-16ab7c1b51af',
        'e3f43197-ac79-4943-8664-1f54bbf95494',
    ];

    //Группы, в которых менялось содержимое от определенного времени (показать строки, которые были недавно перемещены либо изменены или созданы)
    public const SHOW_NOTE_GROUP_ONLY_CHANGED_IDS = [
        '5cb1dc6c-8d98-4be1-a0a1-68ac725199e2',
        '10eecb64-b39f-44db-a1ba-d075378fb6b0',
        '6a36fa70-995f-40b4-86ec-350a8ec20805',
        'b42109ca-864c-467a-9ce7-16ab7c1b51af',
        'e3f43197-ac79-4943-8664-1f54bbf95494',
    ];

    private const FORMATTED_OFFSET = "\t\t\t\t";

    public function __construct(private TrackReportService $trackReportService, private NoteService $noteService, private TgBotClient $client)
    {
    }

    public function minOldUpdatedRowTime(): int
    {
        return Carbon::now()->modify('-1 day')->setTime(15, 25)->getTimestamp();
    }

    /**
     * @return array
     * @throws ValidateException
     * @throws NotFoundException
     */
    public function getDailyReport(): array
    {
        $groups = $this->noteService->getGroups(self::NOTE_ID);

        $reportGroups = [];

        foreach (self::NOTE_GROUP_IDS as $groupId) {
            foreach ($groups as $group) {
                if ($groupId === $group->id) {
                    if (in_array($group->id, self::SHOW_NOTE_GROUP_ONLY_CHANGED_IDS)) {
                        $group->items = array_filter($group->items, fn(Row $row) => max($row->createdAt, (int)$row->updatedAt) > $this->minOldUpdatedRowTime());
                    }
                    if (count($group->items) > 0) {
                        $reportGroups[] = $group;
                    }
                    break;
                }
            }
        }

        return ['trackReport' => $this->trackReportService->makeDailyTreeReport([Track::T_USEFUL, Track::T_UNDEFINED]), 'noteReport' => $reportGroups];
    }

    /**
     * @throws NotFoundException|ValidateException
     */
    public function getFormatedDailyReport(): string
    {
        $report = $this->getDailyReport();

        $formatedReports = [];

        $trackReportHead = Emoji::BAR_CHART . " <b>Статистика задач</b>:";

        $trackReportArr = [];
        if ($report['trackReport']['beforeCurrentDayReport'] ?? false) {
            $dayReport = $report['trackReport']['beforeCurrentDayReport'];
            $trackReportArr[] = self::getFormatedTreeReport($dayReport, Emoji::GREEN_CHECK);
        }

        if ($report['trackReport']['currentDayReport'] ?? false) {
            $dayReport = $report['trackReport']['currentDayReport'];
            $trackReportArr[] = self::getFormatedTreeReport($dayReport, Emoji::GREY_CHECK);
        }

        if (count($trackReportArr) > 0) {
            $formatedReports[] = $trackReportHead . "\n\n" . StringHelper::addBeforeRow(implode("\n\n", $trackReportArr), self::FORMATTED_OFFSET);
        }

        if ($report['noteReport'] ?? false) {
            $noteReport = $report['noteReport'];
            $groupsArr = [];
            foreach ($noteReport as $group) {
                $rowNumber = 1;
                $rowsArr = array_map(function (Row $row) use (&$rowNumber) {
                    return Emoji::PUSH_PIN . ' <b>' . ($rowNumber++) . '</b>. <u>' . $row->name . '</u>';
                }, $group->items);
                $rowsText = StringHelper::addBeforeRow(implode("\n", $rowsArr), self::FORMATTED_OFFSET);
                $groupsArr[] = Emoji::BOARD . " <b>$group->name</b>:\n$rowsText";
            }
            $formatedReports[] = Emoji::NOTEBOOK . "<b> Блокнот</b>:\n\n" . StringHelper::addBeforeRow(implode("\n\n", $groupsArr), self::FORMATTED_OFFSET);
        }

        return implode("\n\n\n\n", $formatedReports);
    }

    /**
     * @throws NotFoundException|ValidateException|GuzzleException|ContainerExceptionInterface|NotFoundExceptionInterface
     */
    public function sendDailyReport(): void
    {
        $message = $this->getFormatedDailyReport();
        $chatId = config()->get('services.tgbot.chatId');
        $this->client->sendMessage((int)$chatId, $message);
    }

    private function getFormatedTreeReport($currentDayReport, string $beforeHead = ""): string
    {
        $dayOfWeak = Str::ucfirst(TimeFormatHelper::getDayOfWeak($currentDayReport->createdAt, TimeFormatHelper::DAY_OF_WEEK_DICT_RU));

        $relativeTenseInWords = TimeFormatHelper::getRelativeTenseInWords($currentDayReport->createdAt, TimeFormatHelper::RELATIVE_TENSE_IN_WORDS_DICT_RU);
        $relativeTenseInWords = $relativeTenseInWords ? '(' . $relativeTenseInWords . ')' : '';
        $createdDateFormat = Carbon::createFromTimestamp($currentDayReport->createdAt)->format('Y-m-d');

        $head = $beforeHead . ' ' . preg_replace('/\s+/ui', ' ', implode(' ', [$dayOfWeak, $relativeTenseInWords, $createdDateFormat])) . ':';

        $formattedTypes = [];

        foreach ($currentDayReport->trees as $treeData) {
            $namedType = [Track::T_USEFUL => 'Полезные задачи', Track::T_UNDEFINED => 'Неопределенные задачи', Track::T_USELESS => 'Бесполезные задачи'][$treeData['type']];
            $iconType = [Track::T_USEFUL => Emoji::GREEN_CIRCLE, Track::T_UNDEFINED => Emoji::WHITE_CIRCLE, Track::T_USELESS => Emoji::BLUE_CIRCLE][$treeData['type']];
            $formattedTree = self::buildFormatedTreeReport($treeData['tree']['children']);
            $formattedTree = $formattedTree ?: (self::FORMATTED_OFFSET . self::FORMATTED_OFFSET . "[Не было]");

            $shortTime = TimeFormatHelper::getFormatedInterval($treeData['tree']['allSeconds'], true, TimeFormatHelper::TIME_FORMAT_INTERVAL_DICT_RU);
            $percent = $treeData['type'] === Track::T_USEFUL ? (' | ' . (round(((double)$treeData['tree']['allSeconds'] / (double)(5 * TimeHelper::HOUR) * 100.0), 1)) . '%') : '';

            $formattedTypes[] = "$iconType <b>$namedType</b> <i>($shortTime$percent)</i>:\n$formattedTree";
        }

        $types = implode("\n\n", $formattedTypes);
        $types = StringHelper::addBeforeRow($types, self::FORMATTED_OFFSET);
        return "<b>$head</b>\n$types";
    }

    private static function buildFormatedTreeReport(array $tree, $offset = self::FORMATTED_OFFSET, int $deep = 0): string
    {
        $textArr = [];
        foreach ($tree as $i => $token) {
            $shortTime = TimeFormatHelper::getFormatedInterval($token['allSeconds'], true, TimeFormatHelper::TIME_FORMAT_INTERVAL_DICT_RU);
            $tokenName = Str::ucfirst($token['token']);
            $head = "<u>$tokenName</u> <i>[$shortTime]</i>:";
            $text = $head;
            if (count($token['children']) > 0) {
                $childText = self::buildFormatedTreeReport($token['children'], $offset, $deep + 1);
                $childText = StringHelper::addBeforeRow($childText, $offset);
                $text = "$head\n$childText";
            }

            $n = $i + 1;

            $pin = Emoji::UL_ICONS[$deep] ?? null;
            $pin = $pin ? "$pin " : '';

            $textArr[] = "$offset$pin<b>$n</b>. $text";
        }
        return implode("\n", $textArr);
    }
}

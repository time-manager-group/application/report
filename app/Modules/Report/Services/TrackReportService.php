<?php

namespace App\Modules\Report\Services;

use App\Entities\Common\Limiter;
use App\Exceptions\ValidateException;
use App\Modules\Report\Components\TrackAggregator;
use App\Modules\Tracker\Entities\Filters\ReportFilter;
use App\Modules\Tracker\Entities\Order\ReportOrder;
use App\Modules\Tracker\Entities\Track;
use App\Modules\Tracker\Repositories\ReportRepository;
use App\Tools\TimeHelper;
use App\Tools\Validator\ValGr;
use App\Tools\Validator\ValidateManager;
use App\Tools\Validator\Validators\Arr;
use App\Tools\Validator\Validators\Range;


class TrackReportService
{
    public function __construct(private ReportRepository $reportRepository)
    {

    }

    public function makeDailyReport(): array
    {
        $currentTime = time();
        //$currentTime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2024-10-10 12:00:00')->timestamp;//TODO убрать. это для тестирования
        $order = fromArr(['createdAt' => false], ReportOrder::class);
        $limit = fromArr(['limit' => 1], Limiter::class);

        $currentDayReports = $this->reportRepository->findReports(fromArr(
            [
                'createdAtFrom' => TimeHelper::startDayTs($currentTime),
                'createdAtTo' => TimeHelper::nextDayTs($currentTime),
            ], ReportFilter::class),
            $order, $limit);
        if (count($currentDayReports) === 0) {
            return [];
        }
        $currentDayReport = $currentDayReports[0];

        $beforeCurrentDayReport = null;
        $beforeCurrentDayReports = $this->reportRepository->findReports(fromArr(
            [
                'idTo' => $currentDayReport->id - 1,
            ], ReportFilter::class),
            $order, $limit);

        if (count($beforeCurrentDayReports) > 0) {
            $beforeCurrentDayReport = $beforeCurrentDayReports[0];
        }

        return ['currentDayReport' => $currentDayReport, 'beforeCurrentDayReport' => $beforeCurrentDayReport];
    }

    /**
     * @param array<string> $types
     * @return array
     * @throws ValidateException
     */
    public function makeDailyTreeReport(array $types): array
    {
        ValidateManager::validateOrThrow([
            'types' => ValGr::m($types, Arr::m()->min(1)->max(count(Track::getTypes()))->each(Range::m()->enums(Track::getTypes()))),
        ]);

        $report = $this->makeDailyReport();
        $treeReport = function ($report, array $types) {
            $reportAggregator = new TrackAggregator($report->tracks);
            unset($report->tracks);
            $reportTree = $report;
            $reportTree->trees = array_map(function (string $type) use ($reportAggregator): array {
                return [
                    'type' => $type,
                    'tree' => $reportAggregator->makeTree($type),
                ];
            }, $types);

            return $reportTree;
        };

        return [
            'currentDayReport' => $treeReport($report['currentDayReport'], $types),
            'beforeCurrentDayReport' => $treeReport($report['beforeCurrentDayReport'], $types),
        ];

    }
}

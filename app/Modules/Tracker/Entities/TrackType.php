<?php

namespace App\Modules\Tracker\Entities;

class TrackType
{
    public int $id;
    public string $name;
    public int $createdAt;
    public int $updatedAt;
    public string $code;
}

<?php

namespace App\Modules\Tracker\Entities;

use Illuminate\Support\Str;

class Track
{
    public const TOKEN_SEPARATOR = ':';
    public const T_USELESS = 'useless';
    public const T_UNDEFINED = 'undefined';
    public const T_USEFUL = 'useful';

    public int $id;
    public int $createdAt;
    public int $updatedAt;
    public string $name;
    public int $dateStart;
    public int $dateEnd;
    public bool $isActive;
    /**
     * @var self::T_USELESS|self::T_UNDEFINED|self::T_USEFUL
     */
    public string $type;
    public int $reportId;

    public function getPreparedName(): string
    {
        return Str::lower(trim(preg_replace('/[.|:]/ui', self::TOKEN_SEPARATOR, preg_replace('/\s+/ui', ' ', $this->name))));
    }

    public function getSeconds(): int
    {
        $dateEnd = $this->isActive ? (time() - (3 * 3600)) : $this->dateEnd;
        return $dateEnd - $this->dateStart;
    }

    /**
     * @return string[]
     */
    public function getTokens(): array
    {
        return array_map(function (string $token): string {
            return trim($token);
        }, explode(self::TOKEN_SEPARATOR, $this->getPreparedName()));
    }

    public static function getTypes(): array
    {
        return [self::T_USEFUL, self::T_UNDEFINED, self::T_USELESS];
    }
}

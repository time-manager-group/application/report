<?php

namespace App\Modules\Tracker\Entities\Filters;

class ReportFilter
{
    /** @var int[] */
    public array $ids = [];
    /** @var int[] */
    public array $notIds = [];
    public int|null $idFrom = null;
    public int|null $idTo = null;
    public int|null $createdAtFrom = null;
    public int|null $createdAtTo = null;
}

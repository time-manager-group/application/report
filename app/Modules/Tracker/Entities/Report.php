<?php

namespace App\Modules\Tracker\Entities;

class Report
{
    public int $id;
    public string $name;
    public int $createdAt;
    public int $updatedAt;
    /**
     * @var Track[]
     */
    public array $tracks;
}

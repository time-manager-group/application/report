<?php

namespace App\Modules\Tracker\Repositories;

use App\Entities\Common\Limiter;
use App\Modules\Tracker\Entities\Filters\ReportFilter;
use App\Modules\Tracker\Entities\Order\ReportOrder;
use App\Modules\Tracker\Entities\Report;
use App\Modules\Tracker\Entities\Track;
use App\Modules\Tracker\Entities\TrackType;
use App\Tools\DbTrait;
use App\Tools\TimeHelper;
use Illuminate\Support\Facades\DB;
use stdClass;

class ReportRepository
{
    use DbTrait;

    private array|null $__trackTypes = null;

    /**
     * @return Report[]
     */
    public function findReports(ReportFilter $filter, ReportOrder $order, Limiter $limiter): array
    {
        $reportQuery = DB::table('reports');

        if (!is_null($filter->createdAtFrom)) {
            $reportQuery->where('created_at', '>=', TimeHelper::tsToDbFormat($filter->createdAtFrom));
        }
        if (!is_null($filter->createdAtTo)) {
            $reportQuery->where('created_at', '<=', TimeHelper::tsToDbFormat($filter->createdAtTo));
        }
        if (count($filter->ids) > 0) {
            $reportQuery->whereIn('id', $filter->ids);
        }
        if (count($filter->notIds) > 0) {
            $reportQuery->whereNotIn('id', $filter->notIds);
        }
        if (!is_null($filter->idFrom)) {
            $reportQuery->where('id', '>=', $filter->idFrom);
        }
        if (!is_null($filter->idTo)) {
            $reportQuery->where('id', '<=', $filter->idTo);
        }

        if (!is_null($order->createdAt)) {
            $reportQuery->orderBy('created_at', $order->createdAt ? 'asc' : 'desc');
        }

        $reports = $this->setLimit($reportQuery, $limiter)->get()->all();
        if (count($reports) === 0) {
            return [];
        }

        $trackQuery = DB::table('tracks');
        $trackQuery->where('report_id', array_map(function (stdClass $item): int {
            return $item->id;
        }, $reports));
        $tracks = $trackQuery->orderBy('id')->get()->all();

        return array_map(function (stdClass $item) use ($tracks): Report {
            return fromArr([
                'id' => $item->id,
                'name' => $item->name,
                'createdAt' => TimeHelper::dbFormatToTs($item->created_at),
                'updatedAt' => TimeHelper::dbFormatToTs($item->created_at),
                'tracks' => array_map(function (stdClass $item): Track {
                    return fromArr([
                        'id' => $item->id,
                        'name' => $item->name,
                        'createdAt' => TimeHelper::dbFormatToTs($item->created_at),
                        'updatedAt' => TimeHelper::dbFormatToTs($item->created_at),
                        'isActive' => $item->is_active,
                        'dateStart' => TimeHelper::dbFormatToTs($item->date_start),
                        'dateEnd' => TimeHelper::dbFormatToTs($item->date_end),
                        'type' => $this->getMapTrackTypes()[$item->type_id]->code,
                        'reportId' => $item->report_id,
                    ], Track::class);
                }, $tracks),
            ], Report::class);
        }, $reports);
    }

    /**
     * @return array<int, TrackType>
     */
    public function getMapTrackTypes(): array
    {
        if (is_null($this->__trackTypes)) {
            $this->__trackTypes = [];
            foreach (DB::table('track_types')->get()->all() as $trackType) {
                $this->__trackTypes[$trackType->id] = fromArr([
                    'id' => $trackType->id,
                    'createdAt' => TimeHelper::dbFormatToTs($trackType->created_at),
                    'updatedAt' => TimeHelper::dbFormatToTs($trackType->updated_at),
                    'name' => $trackType->name,
                    'code' => $trackType->code,
                ], TrackType::class);
            }
        }

        return $this->__trackTypes;
    }
}

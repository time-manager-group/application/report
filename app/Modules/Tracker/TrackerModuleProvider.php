<?php

namespace App\Modules\Tracker;

use App\Modules\Report\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class TrackerModuleProvider extends ServiceProvider
{
    public function boot(): void
    {
    }
}

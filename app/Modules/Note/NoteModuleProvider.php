<?php

namespace App\Modules\Note;

use App\Modules\Note\Http\Controllers\NoteController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class NoteModuleProvider extends ServiceProvider
{
    public function boot(): void
    {
        Route::middleware(['web'])->prefix('note')->group(function () {
            Route::get('/list-groups', [NoteController::class, 'listGroups']);
            Route::post('/add-row-to-after', [NoteController::class, 'addRowToAfter']);
            Route::post('/change-row', [NoteController::class, 'changeRow']);
            Route::post('/delete-row', [NoteController::class, 'deleteRow']);
            Route::post('/move-row', [NoteController::class, 'moveRow']);
            Route::post('/move-row-to-backup', [NoteController::class, 'moveRowToBackup']);
            Route::get('/stat-storages', [NoteController::class, 'statStorages']);
        });
    }
}

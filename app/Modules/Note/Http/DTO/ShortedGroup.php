<?php

namespace App\Modules\Note\Http\DTO;

use App\Modules\Note\Entities\Group;
use App\Modules\Note\Entities\Row;

class ShortedGroup
{
    public string $id;
    public string $name;
    /**
     * @var Row[]
     */
    public array $items;

    public static function fromEntity(Group $group): self
    {
        return fromArr([
            'id' => $group->id,
            'items' => array_map(function (Row $item): ShortedRow {
                return ShortedRow::fromEntity($item);
            }, $group->items),
        ], self::class);
    }
}

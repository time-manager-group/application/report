<?php

namespace App\Modules\Note\Http\DTO;

use App\Modules\Note\Entities\Row;

class ShortedRow
{
    public string $id;
    public string $name;

    public static function fromEntity(Row $row): self
    {
        return fromArr([
            'id' => $row->id,
            'name' => $row->name,
        ], self::class);
    }
}

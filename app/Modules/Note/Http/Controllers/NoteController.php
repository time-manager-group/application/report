<?php

namespace App\Modules\Note\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Exceptions\ValidateException;
use App\Http\Controllers\Controller;
use App\Modules\Note\Entities\Group;
use App\Modules\Note\Http\DTO\ShortedGroup;
use App\Modules\Note\Services\DTO\Row;
use App\Modules\Note\Services\NoteService;
use App\Modules\Note\Services\StorageStatService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class NoteController extends Controller
{
    const NOTE_ID = '3f2a1c88-c719-4f5e-88ff-663830016000';

    public function listGroups(NoteService $noteService): JsonResponse
    {
        try {
            $groups = $noteService->getGroups(self::NOTE_ID);
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), [], []);
        } catch (NotFoundException $e) {
            return $this->notFound($e->getMessage());
        }
        return response()->json([
            'data' => array_map(function (Group $item): ShortedGroup {
                return ShortedGroup::fromEntity($item);
            }, $groups),
        ]);
    }

    /**
     * @throws Throwable
     */
    public function addRowToAfter(NoteService $noteService, Request $request): JsonResponse
    {
        try {
            $this->validateRequiredParams($request, ['groupId']);
            $this->validateStringParams($request, ['groupId', 'afterRowId']);
            $noteService->addRowToAfter(self::NOTE_ID, $request->get('groupId'), $request->get('afterRowId'), fromArr([
                'id' => $request->json('id'),
                'name' => $request->json('name'),
            ], Row::class));
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), ['groupId', 'afterRowId']);
        } catch (NotFoundException $e) {
            return $this->notFound($e->getMessage());
        }
        return response()->json();
    }

    /**
     * @throws Throwable
     */
    public function changeRow(NoteService $noteService, Request $request): JsonResponse
    {
        try {
            $this->validateRequiredStringParams($request, ['groupId', 'rowId']);
            $noteService->changeRow(self::NOTE_ID, $request->get('groupId'), $request->get('rowId'), fromArr([
                'id' => $request->json('id'),
                'name' => $request->json('name'),
            ], Row::class));
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), ['groupId', 'rowId']);
        } catch (NotFoundException $e) {
            return $this->notFound($e->getMessage());
        }
        return response()->json();
    }

    public function deleteRow(NoteService $noteService, Request $request): JsonResponse
    {
        try {
            $this->validateRequiredStringParams($request, ['groupId', 'rowId']);
            $noteService->deleteRow(self::NOTE_ID, $request->get('groupId'), $request->get('rowId'));
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), ['groupId', 'rowId'], []);
        } catch (NotFoundException $e) {
            return $this->notFound($e->getMessage());
        }
        return response()->json();
    }

    public function moveRow(NoteService $noteService, Request $request): JsonResponse
    {
        try {
            $this->validateRequiredParams($request, ['groupId', 'rowId', 'toGroupId']);
            $this->validateStringParams($request, ['groupId', 'rowId', 'toGroupId', 'afterRowId']);
            $noteService->moveRow(self::NOTE_ID, $request->get('groupId'), $request->get('rowId'), self::NOTE_ID, $request->get('toGroupId'), $request->get('afterRowId'));
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), ['groupId', 'rowId', 'toGroupId', 'afterRowId'], []);
        } catch (NotFoundException $e) {
            return $this->notFound($e->getMessage());
        }
        return response()->json();
    }

    public function moveRowToBackup(NoteService $noteService, Request $request): JsonResponse
    {
        try {
            $this->validateRequiredStringParams($request, ['groupId', 'rowId']);
            $noteService->moveRowToBackup(self::NOTE_ID, $request->get('groupId'), $request->get('rowId'));
        } catch (ValidateException $e) {
            return $this->validateErrorResponse($e->getErrors(), ['groupId', 'rowId'], []);
        } catch (NotFoundException $e) {
            return $this->notFound($e->getMessage());
        }
        return response()->json();
    }

    public function statStorages(StorageStatService $statService): JsonResponse
    {
        $stats = $statService->getAllStat();
        return response()->json([
            'data' => $stats,
        ]);
    }
}

<?php

namespace App\Modules\Note\Repositories;

use App\Modules\Note\Entities\GroupActionLog;
use App\Modules\Note\Services\DTO\StorageStatInfo;
use App\Tools\DbTrait;
use App\Tools\TimeHelper;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class NoteGroupActionLogRepository
{
    use DbTrait;

    public function addLog(GroupActionLog $payload): int
    {
        $payloadAttribute = [];
        if (!is_null($payload->row)) {
            $payloadAttribute['row'] = $payload->row->toArray();
        }
        if (!is_null($payload->group)) {
            $payloadAttribute['group'] = Arr::only($payload->group->toArray(), ['id', 'createdAt', 'updatedAt', 'items']);
        }

        return DB::table('note_group_action_logs')
            ->insert([
                'time' => TimeHelper::tsToDbFormat($payload->time),
                'request_id' => $payload->requestId,
                'action' => $payload->action,
                'note_id' => $payload->noteId,
                'group_id' => $payload->groupId,
                'row_id' => $payload->rowId,
                'params' => json_encode($payload->params),
                'payload' => json_encode($payloadAttribute),
            ]);
    }

    public function getStat(): StorageStatInfo
    {
        return self::getTableStat('note_group_action_logs');
    }
}

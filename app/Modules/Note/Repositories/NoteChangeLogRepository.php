<?php

namespace App\Modules\Note\Repositories;

use App\Modules\Note\Entities\Group;
use App\Modules\Note\Entities\NoteChangeLog;
use App\Modules\Note\Services\DTO\StorageStatInfo;
use App\Tools\DbTrait;
use App\Tools\TimeHelper;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class NoteChangeLogRepository
{
    use DbTrait;

    public function addLog(NoteChangeLog $payload): int
    {
        $body = [];

        $body['groups'] = array_map(function (Group $item): array {
            return Arr::only($item->toArray(), ['id', 'createdAt', 'updatedAt', 'items']);
        }, $payload->groups);

        return DB::table('note_change_logs')
            ->insert([
                'time' => TimeHelper::tsToDbFormat($payload->time),
                'request_id' => $payload->requestId,
                'note_id' => $payload->noteId,
                'body' => json_encode($body),
            ]);
    }

    public function getStat(): StorageStatInfo
    {
        return self::getTableStat('note_change_logs');
    }
}

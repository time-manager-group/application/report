<?php

namespace App\Modules\Note\Repositories;

use App\Modules\Note\Entities\Group;
use App\Modules\Note\Entities\Note;
use App\Modules\Note\Entities\Row;
use App\Tools\DbTrait;
use App\Tools\TimeHelper;
use Illuminate\Support\Facades\DB;
use stdClass;

class NoteRepository
{
    use DbTrait;

    public function findNote(string $id): ?Note
    {
        $note = DB::table('notes')
            ->where('id', $id)
            ->get()->first();
        if (is_null($note)) {
            return null;
        }

        return fromArr([
            'id' => $note->id,
            'name' => $note->name,
            'createdAt' => TimeHelper::dbFormatToTs($note->created_at),
            'updatedAt' => TimeHelper::dbFormatToTs($note->updated_at),
        ], Note::class);
    }

    public function updateNote(string $id, Note $payload): int
    {
        return DB::table('notes')
            ->where('id', $id)
            ->update([
                'id' => $payload->id,
                'name' => $payload->name,
                'created_at' => TimeHelper::tsToDbFormat($payload->createdAt),
                'updated_at' => TimeHelper::tsToDbFormat($payload->updatedAt),
            ]);
    }

    /**
     * @return Group[]
     */
    public function findGroups(string $noteId): array
    {
        $groups = DB::table('note_groups')
            ->where('note_id', $noteId)
            ->get()->all();

        return array_map(function (stdClass $group): Group {
            return self::serializeGroupFromDb($group);
        }, $groups);
    }

    public function findGroup(string $noteId, string $groupId): ?Group
    {
        $group = DB::table('note_groups')
            ->where('note_id', $noteId)
            ->where('id', $groupId)
            ->get()->first();
        if (is_null($group)) {
            return null;
        }

        return self::serializeGroupFromDb($group);
    }

    public function updateGroup(string $noteId, string $groupId, Group $payload): int
    {
        return DB::table('note_groups')
            ->where('note_id', $noteId)
            ->where('id', $groupId)
            ->update([
                'id' => $payload->id,
                'note_id' => $payload->noteId,
                'name' => $payload->name,
                'created_at' => TimeHelper::tsToDbFormat($payload->createdAt),
                'updated_at' => TimeHelper::tsToDbFormat($payload->updatedAt),
                'row_items' => json_encode(array_map(function (Row $item): array {
                    return $item->toArray();
                }, $payload->items)),
            ]);
    }

    private static function serializeGroupFromDb(stdClass $group): Group
    {
        return fromArr([
            'id' => $group->id,
            'noteId' => $group->note_id,
            'name' => $group->name,
            'createdAt' => TimeHelper::dbFormatToTs($group->created_at),
            'updatedAt' => TimeHelper::dbFormatToTs($group->updated_at),
            'items' => array_map(function (array $item): Row {
                return Row::fromArr($item);
            }, json_decode($group->row_items, true)),
        ], Group::class);
    }
}

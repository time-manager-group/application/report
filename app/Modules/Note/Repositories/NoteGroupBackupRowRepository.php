<?php

namespace App\Modules\Note\Repositories;

use App\Entities\Common\Limiter;
use App\Modules\Note\Entities\BackupRow;
use App\Modules\Note\Entities\Filters\BackupRowFilter;
use App\Modules\Note\Entities\Group;
use App\Modules\Note\Entities\Row;
use App\Modules\Note\Services\DTO\StorageStatInfo;
use App\Tools\DbTrait;
use App\Tools\TimeHelper;
use Illuminate\Support\Facades\DB;
use stdClass;

class NoteGroupBackupRowRepository
{
    use DbTrait;

    public function addRow(BackupRow $payload): int
    {
        $row = $payload->row;

        return DB::table('note_group_backup_rows')
            ->insert([
                'time' => TimeHelper::tsToDbFormat($payload->time),
                'note_id' => $payload->noteId,
                'group_id' => $payload->groupId,
                'row_id' => $payload->rowId,
                'row_name' => $payload->rowName,
                'meta' => json_encode([
                    'row' => $row->toArray(),
                ]),
            ]);
    }

    /**
     * @param BackupRowFilter $filter
     * @param Limiter         $limiter
     * @return BackupRow[]
     */
    public function findRows(BackupRowFilter $filter, Limiter $limiter): array
    {
        $query = DB::table('note_group_backup_rows');

        if (!is_null($filter->noteId)) {
            $query->where('note_id', $filter->noteId);
        }
        if (!is_null($filter->groupId)) {
            $query->where('group_id', $filter->groupId);
        }
        if (!is_null($filter->rowId)) {
            $query->where('row_id', $filter->rowId);
        }

        $groups = $this->setLimit($query, $limiter)->get()->all();

        return array_map(function (stdClass $item): Group {
            $meta = json_decode($item->meta, true);
            return fromArr([
                'time' => TimeHelper::dbFormatToTs($item->time),
                'note_id' => $item->noteId,
                'group_id' => $item->groupId,
                'row_id' => $item->rowId,
                'row' => array_key_exists('row', $meta) ? Row::fromArr($meta['row']) : null,
            ], BackupRow::class);
        }, $groups);
    }

    public function getStat(): StorageStatInfo
    {
        return self::getTableStat('note_group_backup_rows');
    }
}

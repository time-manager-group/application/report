<?php

namespace App\Modules\Note\Services\DTO;

class StorageStatInfo
{
    public int $rowsCount = 0;
    public int $sizeByte = 0;
    public int $rowOdlTime = 0;
    public int $rowLastTime = 0;
}

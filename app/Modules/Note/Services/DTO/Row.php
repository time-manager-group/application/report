<?php

namespace App\Modules\Note\Services\DTO;

class Row
{
    public mixed $id;
    public mixed $name;

    public function makeEntity(): \App\Modules\Note\Entities\Row
    {
        $row = new \App\Modules\Note\Entities\Row();
        $row->id = $this->id;
        $row->name = $this->name;
        return $row;
    }
}

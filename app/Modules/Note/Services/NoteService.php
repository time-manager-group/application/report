<?php

namespace App\Modules\Note\Services;

use App\Entities\Common\Limiter;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidateException;
use App\Modules\Note\Entities\Filters\BackupRowFilter;
use App\Modules\Note\Entities\Group;
use App\Modules\Note\Entities\GroupActionLog;
use App\Modules\Note\Entities\Note;
use App\Modules\Note\Entities\NoteChangeLog;
use App\Modules\Note\Entities\Row;
use App\Modules\Note\Repositories\NoteChangeLogRepository;
use App\Modules\Note\Repositories\NoteGroupActionLogRepository;
use App\Modules\Note\Repositories\NoteGroupBackupRowRepository;
use App\Modules\Note\Repositories\NoteRepository;
use App\Tools\SystemHelper;
use App\Tools\Validator\ValGr;
use App\Tools\Validator\ValidateManager;
use App\Tools\Validator\Validators\Cmp;
use App\Tools\Validator\Validators\Req;
use App\Tools\Validator\Validators\Str;
use App\Tools\Validator\Validators\Uniq;
use App\Tools\Validator\Validators\Uuid;
use Carbon\Carbon;
use RuntimeException;
use Throwable;

class NoteService
{
    public function __construct(
        private NoteRepository $noteRepository,
        private NoteGroupBackupRowRepository $backupRowRepository,
        private NoteGroupActionLogRepository $actionLogRepository,
        private NoteChangeLogRepository $changeLogRepository,
    ) {
    }

    /**
     * @param string $noteId
     * @return Group[]
     * @throws NotFoundException
     * @throws ValidateException
     */
    public function getGroups(string $noteId): array
    {
        $this->loadNote($noteId);
        return $this->noteRepository->findGroups($noteId);
    }

    /**
     * @throws NotFoundException|ValidateException|Throwable
     */
    public function addRowToAfter(string $noteId, string $groupId, ?string $afterRowId, DTO\Row $payload): void
    {
        $note = $this->loadNote($noteId);
        $group = $this->loadGroup($noteId, $groupId);

        ValidateManager::validateOrThrow([
            'afterRowId' => ValGr::m($afterRowId, Uuid::m())->mustEmpty(),
        ]);
        if ($afterRowId && !in_array($afterRowId, $group->getItemIds(), true)) {
            NotFoundException::throw('after row not found');
        }

        ValidateManager::validateOrAddErrorsAndThrow('payload', [
            'id' => ValGr::m($payload->id, [Req::m(), Uuid::m(), Uniq::m()->enums($group->getItemIds())]),
            'name' => ValGr::m($payload->name, [Req::m(), Str::m()->min(1)]),
        ]);

        $newRow = fromArr([
            'createdAt' => Carbon::now()->getTimestamp(),
        ], $payload->makeEntity());

        $group = $this->insertRowAfter($group, $afterRowId, $newRow);

        $actionLog = $this->makeActionLog(__FUNCTION__, $noteId, $groupId, $newRow, ['noteId' => $noteId, 'groupId' => $groupId, 'afterRowId' => $afterRowId]);

        $this->updateGroup($note, $group, $actionLog);
    }

    /**
     * @throws NotFoundException|ValidateException|Throwable
     */
    public function changeRow(string $noteId, string $groupId, string $rowId, DTO\Row $payload): void
    {
        $note = $this->loadNote($noteId);
        $group = $this->loadGroup($noteId, $groupId);

        ValidateManager::validateOrThrow([
            'rowId' => ValGr::m($rowId, Uuid::m()),
        ]);
        if (!in_array($rowId, $group->getItemIds(), true)) {
            NotFoundException::throw('row not found');
        }

        ValidateManager::validateOrAddErrorsAndThrow('payload', [
            'id' => ValGr::m($payload->id, [Uuid::m(), Uniq::m()->enums($group->getItemIds())->excluded($rowId)])->nullable(),
            'name' => ValGr::m($payload->name, Str::m()->min(1))->nullable(),
        ]);

        $foundedGroupHash = $group->getHash();

        $newItems = [];
        $newRow = null;
        foreach ($group->items as $row) {
            if ($row->id === $rowId) {
                $newRow = fromArr([
                    'id' => $payload->id,
                    'name' => $payload->name,
                    'updatedAt' => Carbon::now()->getTimestamp(),
                    'updatedAttrAt' => Carbon::now()->getTimestamp(),
                ], $row, true);

                $newItems[] = $newRow;
            } else {
                $newItems[] = $row;
            }
        }

        $group = fromArr([
            'updatedAt' => Carbon::now()->getTimestamp(),
            'items' => $newItems,
        ], $group);

        if ($foundedGroupHash === $group->getHash()) {
            return;
        }

        $actionLog = $this->makeActionLog(__FUNCTION__, $noteId, $groupId, $newRow, ['noteId' => $noteId, 'groupId' => $groupId, 'rowId' => $rowId]);

        $this->updateGroup($note, $group, $actionLog);
    }

    /**
     * @throws NotFoundException|ValidateException
     */
    public function deleteRow(string $noteId, string $groupId, string $rowId): void
    {
        $note = $this->loadNote($noteId);
        $group = $this->loadGroup($noteId, $groupId);

        ValidateManager::validateOrThrow([
            'rowId' => ValGr::m($rowId, Uuid::m()),
        ]);
        if (!in_array($rowId, $group->getItemIds(), true)) {
            NotFoundException::throw('row not found');
        }

        [$group, $deletedRow] = $this->removeRow($group, $rowId);

        $actionLog = $this->makeActionLog(__FUNCTION__, $noteId, $groupId, $deletedRow, ['noteId' => $noteId, 'groupId' => $groupId, 'rowId' => $rowId]);

        $this->updateGroup($note, $group, $actionLog);
    }

    /**
     * @throws NotFoundException|ValidateException
     */
    public function moveRow(string $noteId, string $groupId, string $rowId, string $toNoteId, string $toGroupId, ?string $afterRowId): void
    {
        $fromNote = $this->loadNote($noteId);
        $fromGroup = $this->loadGroup($noteId, $groupId);
        ValidateManager::validateOrThrow([
            'rowId' => ValGr::m($rowId, Uuid::m()),
        ]);
        if (!in_array($rowId, $fromGroup->getItemIds(), true)) {
            NotFoundException::throw('row not found');
        }

        if ($noteId === $toNoteId) {  // В один и тот же блокнот
            $toNote = $fromNote;
        } else {
            $toNote = $this->loadNote($toNoteId);
        }
        if ($noteId === $toNoteId && $groupId === $toGroupId) { // В один и тот же блокнот и группу
            $toGroup = $fromGroup;
        } else {
            $toGroup = $this->loadGroup($toNoteId, $toGroupId);
        }
        ValidateManager::validateOrThrow([
            'afterRowId' => ValGr::m($afterRowId, [Uuid::m(), Cmp::m()->comparable($rowId)->same(false)])->mustEmpty(),
        ]);
        if ($afterRowId && !in_array($afterRowId, $toGroup->getItemIds(), true)) {
            NotFoundException::throw('after row not found');
        }

        //Если это внешнее перемещение, то надо убедиться в том, что в другой группе нет с там же id строки
        if (!$fromGroup->isSamePrimary($toGroup) && in_array($rowId, $toGroup->getItemIds(), true)) {
            throw new RuntimeException('Conflict: row with same id already exists in destination group');
        }

        $foundedGroupHash = $fromGroup->getHash();

        // From
        [$fromGroup, $movingRow] = $this->removeRow($fromGroup, $rowId);

        if ($fromGroup->isSamePrimary($toGroup)) {
            fromArr([
                'movedInAt' => Carbon::now()->getTimestamp(),
            ], $movingRow);
        } else {
            fromArr([
                'movedExAt' => Carbon::now()->getTimestamp(),
            ], $movingRow);
        }

        // To
        $toGroup = $this->insertRowAfter($toGroup, $afterRowId, fromArr([
            'updatedAt' => Carbon::now()->getTimestamp(),
        ], $movingRow));

        if ($foundedGroupHash === $fromGroup->getHash()) {
            return;
        }

        $toActionLog = null;
        $actionLogParams = ['noteId' => $noteId, 'groupId' => $groupId, 'rowId' => $rowId, 'toNoteId' => $toNoteId, 'toGroupId' => $toGroupId, 'afterRowId' => $afterRowId];
        if ($fromGroup->isSamePrimary($toGroup)) {
            $fromActionLog = $this->makeActionLog(__FUNCTION__ . '.moveInternal', $noteId, $fromGroup->id, $movingRow, $actionLogParams);
            $fromActionLog->group = clone $fromGroup;
        } else {
            $fromActionLog = $this->makeActionLog(__FUNCTION__ . '.removeRow', $noteId, $fromGroup->id, $movingRow, $actionLogParams);
            $fromActionLog->group = clone $fromGroup;

            $toActionLog = $this->makeActionLog(__FUNCTION__ . '.addRowToAfter', $toNoteId, $toGroup->id, $movingRow, $actionLogParams);
            $toActionLog->group = clone $toGroup;
        }

        $fromNote = fromArr([
            'updatedAt' => Carbon::now()->getTimestamp(),
        ], $fromNote);
        $toNote = fromArr([
            'updatedAt' => Carbon::now()->getTimestamp(),
        ], $toNote);

        $this->noteRepository->transaction(function () use ($fromNote, $toNote, $fromGroup, $toGroup, $fromActionLog, $toActionLog) {
            (function () use ($fromNote, $toNote) {
                if (!$this->noteRepository->updateNote($fromNote->id, $fromNote)) {
                    throw new RuntimeException('no affected rows');
                }
                if ($fromNote->isSamePrimary($toNote)) { // Один тот же блокнот
                    return;
                }
                if (!$this->noteRepository->updateNote($toNote->id, $toNote)) {
                    throw new RuntimeException('no affected rows');
                }
            })();

            (function () use ($fromGroup, $toGroup, $fromActionLog, $toActionLog) {
                if (!$this->noteRepository->updateGroup($fromGroup->noteId, $fromGroup->id, $fromGroup)) {
                    throw new RuntimeException('no affected rows');
                }
                if (!$this->actionLogRepository->addLog($fromActionLog)) {
                    throw new RuntimeException('no affected rows');
                }
                if ($fromGroup->isSamePrimary($toGroup)) { // Одна и та же группа в блокноте
                    return;
                }

                if (!$this->noteRepository->updateGroup($toGroup->noteId, $toGroup->id, $toGroup)) {
                    throw new RuntimeException('no affected rows');
                }
                if (!$this->actionLogRepository->addLog($toActionLog)) {
                    throw new RuntimeException('no affected rows');
                }
            })();

            (function () use ($fromNote, $toNote) {
                $this->addChangeLog($fromNote->id);
                if ($fromNote->isSamePrimary($toNote)) { // Один тот же блокнот
                    return;
                }
                $this->addChangeLog($toNote->id);
            })();
        });
    }

    /**
     * @throws NotFoundException|ValidateException
     */
    public function moveRowToBackup(string $noteId, string $groupId, string $rowId): void
    {
        $note = $this->loadNote($noteId);
        $group = $this->loadGroup($noteId, $groupId);

        ValidateManager::validateOrThrow([
            'rowId' => ValGr::m($rowId, Uuid::m()),
        ]);
        if (!in_array($rowId, $group->getItemIds(), true)) {
            NotFoundException::throw('row not found');
        }

        if (count($this->backupRowRepository->findRows(
                fromArr(['noteId' => $noteId, 'groupId' => $groupId, 'rowId' => $rowId], BackupRowFilter::class),
                fromArr(['limit' => 1], Limiter::class))) > 0) {
            throw new RuntimeException('Conflict: row with same id already exists in row backup');
        }

        [$group, $movingRow] = $this->removeRow($group, $rowId);

        $backupRow = fromArr([
            'time' => Carbon::now()->getTimestamp(),
            'noteId' => $group->noteId,
            'groupId' => $group->id,
        ], $movingRow->makeBackupRow());

        $actionLog = $this->makeActionLog(__FUNCTION__, $noteId, $groupId, $movingRow, ['noteId' => $noteId, 'groupId' => $groupId, 'rowId' => $rowId]);

        $this->backupRowRepository->transaction(function () use ($note, $group, $backupRow, $actionLog) {
            $this->updateGroup($note, $group, $actionLog);
            $this->backupRowRepository->addRow($backupRow);
        });
    }

    private function insertRowAfter(Group $group, ?string $afterRowId, Row $newRow): Group
    {
        $newToGroupItems = [];
        if (!$afterRowId) {
            $newToGroupItems[] = $newRow;
        }
        foreach ($group->items as $row) {
            $newToGroupItems[] = $row;
            if ($afterRowId && $row->id === $afterRowId) {
                $newToGroupItems[] = $newRow;
            }
        }

        return fromArr([
            'updatedAt' => Carbon::now()->getTimestamp(),
            'items' => $newToGroupItems,
        ], $group);
    }

    /**
     * @param Group  $group
     * @param string $rowId
     * @return array{0:Group, 1:Row}
     */
    private function removeRow(Group $group, string $rowId): array
    {
        $newItems = [];
        $movingRow = null;
        foreach ($group->items as $row) {
            if ($row->id === $rowId) {
                if (!is_null($movingRow)) {
                    throw new RuntimeException('moving row already exists');
                }
                $movingRow = $row;
            } else {
                $newItems[] = $row;
            }
        }
        if (is_null($movingRow)) {
            throw new RuntimeException('moving row not found');
        }

        $group = fromArr([
            'updatedAt' => Carbon::now()->getTimestamp(),
            'items' => $newItems,
        ], $group);

        return [$group, $movingRow];
    }

    /**
     * @throws NotFoundException|ValidateException
     */
    private function loadGroup(string $noteId, string $groupId): Group
    {
        ValidateManager::validateOrThrow([
            'noteId' => ValGr::m($noteId, Uuid::m()),
            'groupId' => ValGr::m($groupId, Uuid::m()),
        ]);

        $group = $this->noteRepository->findGroup($noteId, $groupId);
        NotFoundException::throwIfNull($group, 'group not found');

        return $group;
    }

    /**
     * @throws NotFoundException|ValidateException
     */
    private function loadNote(string $noteId): Note
    {
        ValidateManager::validateOrThrow([
            'noteId' => ValGr::m($noteId, Uuid::m()),
        ]);

        $note = $this->noteRepository->findNote($noteId);
        NotFoundException::throwIfNull($note, 'note not found');

        return $note;
    }

    private function updateGroup(Note $note, Group $group, GroupActionLog $actionLog): void
    {
        $note = fromArr([
            'requestId' => '',
            'updatedAt' => Carbon::now()->getTimestamp(),
        ], $note);

        $actionLog = fromArr([
            'group' => $group,
        ], $actionLog);

        $this->noteRepository->transaction(function () use ($note, $group, $actionLog) {
            if (!$this->noteRepository->updateNote($note->id, $note)) {
                throw new RuntimeException('no affected rows');
            }
            if (!$this->noteRepository->updateGroup($group->noteId, $group->id, $group)) {
                throw new RuntimeException('no affected rows');
            }
            if (!$this->actionLogRepository->addLog($actionLog)) {
                throw new RuntimeException('no affected rows');
            }
            $this->addChangeLog($note->id);
        });
    }

    private function makeActionLog($action, $noteId, $groupId, Row|null $row, array $params = []): GroupActionLog
    {
        return fromArr([
            'requestId' => SystemHelper::getRequestId(),
            'time' => Carbon::now()->getTimestamp(),
            'noteId' => $noteId,
            'action' => $action,
            'groupId' => $groupId,
            'rowId' => $row->id ?? null,
            'params' => $params,
            'row' => $row,
        ], GroupActionLog::class);
    }

    private function addChangeLog(string $noteId): void
    {
        $changeLog = fromArr([
            'requestId' => SystemHelper::getRequestId(),
            'time' => Carbon::now()->getTimestamp(),
            'noteId' => $noteId,
            'groups' => $this->noteRepository->findGroups($noteId),
        ], NoteChangeLog::class);

        $this->changeLogRepository->addLog($changeLog);
    }
}

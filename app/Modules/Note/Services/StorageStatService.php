<?php

namespace App\Modules\Note\Services;

use App\Modules\Note\Repositories\NoteChangeLogRepository;
use App\Modules\Note\Repositories\NoteGroupActionLogRepository;
use App\Modules\Note\Repositories\NoteGroupBackupRowRepository;
use App\Modules\Note\Services\DTO\StorageStatInfo;

class StorageStatService
{
    public function __construct(
        private NoteGroupBackupRowRepository $backupRowRepository,
        private NoteGroupActionLogRepository $actionLogRepository,
        private NoteChangeLogRepository $changeLogRepository,
    ) {
    }

    /**
     * @return array<string, StorageStatInfo>
     */
    public function getAllStat(): array
    {
        return [
            'group_action_log_storage' => $this->actionLogRepository->getStat(),
            'change_log_storage' => $this->changeLogRepository->getStat(),
            'group_backup_row_storage' => $this->backupRowRepository->getStat(),
        ];
    }
}

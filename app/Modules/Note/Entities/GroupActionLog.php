<?php

namespace App\Modules\Note\Entities;

class GroupActionLog
{
    public int $time;
    public string $requestId;
    public string $action;
    public string $noteId;
    public string $groupId;
    public string $rowId;
    public array $params;
    public Group|null $group  = null;
    public Row|null $row  = null;
}

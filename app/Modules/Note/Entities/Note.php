<?php

namespace App\Modules\Note\Entities;

class Note
{
    public string $id;
    public string $name;
    public int $createdAt;
    public int|null $updatedAt  = null;

    public function isSamePrimary(self $note): bool
    {
        return $this->id === $note->id;
    }
}

<?php

namespace App\Modules\Note\Entities\Filters;

class BackupRowFilter
{
    public string|null $noteId = null;
    public string|null $groupId = null;
    public string|null $rowId = null;
}

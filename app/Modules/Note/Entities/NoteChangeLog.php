<?php

namespace App\Modules\Note\Entities;

class NoteChangeLog
{
    public int $time;
    public string $requestId;
    public string $noteId;
    /**
     * @var Group[]
     */
    public array $groups;
}

<?php

namespace App\Modules\Note\Entities;

class BackupRow
{
    public int $time;
    public string $noteId;
    public string $groupId;
    public string $rowId;
    public string $rowName;
    public Row $row;
}

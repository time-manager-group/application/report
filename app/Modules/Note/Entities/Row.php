<?php

namespace App\Modules\Note\Entities;

class Row
{
    public string $id;
    public string $name;
    public int $createdAt;
    public int|null $updatedAt = null;
    public int|null $updatedAttrAt = null;
    public int|null $movedInAt = null;
    public int|null $movedExAt = null;

    public function getHash(): string
    {
        return md5(json_encode(['id' => $this->id, 'name' => $this->name]));
    }

    public function makeBackupRow(): BackupRow
    {
        return fromArr([
            'rowId' => $this->id,
            'rowName' => $this->name,
            'row' => clone $this,
        ], BackupRow::class);
    }

    public static function fromArr(array $arr): self
    {
        return fromArr([
            'id' => $arr['id'],
            'name' => $arr['name'],
            'createdAt' => $arr['createdAt'],
            'updatedAt' => $arr['updatedAt'],
            'updatedAttrAt' => $arr['updatedAttrAt'],
            'movedInAt' => $arr['movedInAt'],
            'movedExAt' => $arr['movedExAt'],
        ], self::class);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'updatedAttrAt' => $this->updatedAttrAt,
            'movedInAt' => $this->movedInAt,
            'movedExAt' => $this->movedExAt,
        ];
    }
}

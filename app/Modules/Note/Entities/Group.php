<?php

namespace App\Modules\Note\Entities;

class Group
{
    public string $id;
    public string $noteId;
    public string $name;
    public int $createdAt;
    public int|null $updatedAt = null;
    /**
     * @var Row[]
     */
    public array $items;

    /**
     * @return string[]
     */
    public function getItemIds(): array
    {
        return array_column($this->items, 'id');
    }

    public function getHash(): string
    {
        return md5(json_encode([
            'id' => $this->id,
            'noteId' => $this->noteId,
            'name' => $this->name,
            'items' => array_map(function (Row $item): string {
                return $item->getHash();
            }, $this->items),
        ]));
    }

    public function isSamePrimary(self $group): bool
    {
        return $this->noteId === $group->noteId && $this->id === $group->id;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'noteId' => $this->noteId,
            'name' => $this->name,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'items' => array_map(function (Row $item): array {
                return $item->toArray();
            }, $this->items),
        ];
    }

    public function __clone(): void
    {
        $this->items = array_map(function (Row $item): Row {
            return clone $item;
        }, $this->items);
    }
}

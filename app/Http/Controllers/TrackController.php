<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class TrackController extends Controller
{
    public function getList(int $reportId): Response
    {
        Cache::forever('lastLoadedReportId', $reportId);

        $tracks = DB::table('tracks')
            ->addSelect('tracks.id')
            ->addSelect('tracks.name')
            ->addSelect('tracks.date_start')
            ->addSelect('tracks.date_end')
            ->addSelect('tracks.is_active')
            ->addSelect('track_types.code')
            ->leftJoin('track_types', 'tracks.type_id', 'track_types.id')
            ->orderBy('tracks.id')
            ->where('tracks.report_id', $reportId)
            ->get();

        $result = [];

        foreach ($tracks as $track) {
            $result[] = [
                'id'        => $track->id,
                'name'      => $track->name,
                'dateStart' => $track->date_start,
                'dateEnd'   => $track->date_end,
                'isActive'  => (bool)$track->is_active,
                'type'      => $track->code,
            ];
        }

        return $this->serializer->serialize($result);
    }

    /**
     * @throws Exception
     */
    public function create(int $reportId, Request $request): Response
    {
        $all = $request->all();

        $name      = Arr::get($all, 'name');
        $type      = Arr::get($all, 'type');
        $dateStart = Arr::get($all, 'dateStart');
        $dateEnd   = Arr::get($all, 'dateEnd');
        $isActive  = Arr::get($all, 'isActive');

        $this->checkDuplicateActive($reportId, $isActive);

        $typeItemList = DB::table('track_types')
            ->select(['id'])
            ->where('code', $type)
            ->get()
            ->toArray();

        $typeItem = array_shift($typeItemList);

        $newTrackId = DB::table('tracks')
            ->insertGetId([
                'name'       => $name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date_start' => Carbon::parse($dateStart, 'UTC'),
                'date_end'   => $dateEnd ? Carbon::parse($dateEnd, 'UTC') : null,
                'type_id'    => (int)$typeItem->id,
                'is_active'  => (bool)$isActive,
                'report_id'  => $reportId,
            ]);

        if ($newTrackId) {
            DB::table('reports')
                ->where('id', $reportId)
                ->update(['updated_at' => Carbon::now()]);
        }

        return $this->serializer->serialize(['id' => $newTrackId]);
    }

    /**
     * @throws Exception
     */
    public function update(int $reportId, int $trackId, Request $request): Response
    {
        $all = $request->all();

        $name      = Arr::get($all, 'name');
        $type      = Arr::get($all, 'type');
        $dateStart = Arr::get($all, 'dateStart');
        $dateEnd   = Arr::get($all, 'dateEnd');
        $isActive  = Arr::get($all, 'isActive');

        $this->checkDuplicateActive($reportId, $isActive, $trackId);

        $typeItemList = DB::table('track_types')
            ->select(['id'])
            ->where('code', $type)
            ->get()
            ->toArray();

        $typeItem = array_shift($typeItemList);

        $affected = DB::table('tracks')
            ->where('id', $trackId)
            ->where('report_id', $reportId)
            ->update([
                'name'       => $name,
                'updated_at' => Carbon::now(),
                'date_start' => Carbon::parse($dateStart),
                'date_end'   => $dateEnd ? Carbon::parse($dateEnd) : null,
                'type_id'    => (int)$typeItem->id,
                'is_active'  => (bool)$isActive,
            ]);

        if (!$affected) {
            throw new Exception('not affected rows');
        }

        DB::table('reports')
            ->where('id', $reportId)
            ->update(['updated_at' => Carbon::now()]);

        return $this->serializer->serialize([]);
    }

    public function delete(int $reportId, int $trackId): Response
    {
        $affected = DB::table('tracks')
            ->where('id', $trackId)
            ->where('report_id', $reportId)
            ->delete();

        if (!$affected) {
            throw new Exception('not affected rows');
        }

        DB::table('reports')
            ->where('id', $reportId)
            ->update(['updated_at' => Carbon::now()]);

        return $this->serializer->serialize([]);
    }

    private function checkDuplicateActive(int $reportId, bool $isActive, int $trackId = null): void
    {
        if (!$isActive) {
            return;
        }

        $query = DB::table('tracks')
            ->addSelect('id')
          //  ->where('report_id', $reportId)
            ->where('is_active', true);

        if ($trackId !== null) {
            $query->where('id', '!=', $trackId);
        }

        $count = $query->get()
            ->count();

        if ($count > 0) {
            throw new Exception('conflict: active track must be one');
        }
    }
}

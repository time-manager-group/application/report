<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidateException;
use App\Tools\Serializer;
use App\Tools\Validator\ValGr;
use App\Tools\Validator\ValidateManager;
use App\Tools\Validator\Validators\Req;
use App\Tools\Validator\Validators\Str;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use RuntimeException;

/**
 * Class Controller.
 * Базовый контроллер всех контроллеров.
 *
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     *
     * @param Serializer $serializer
     */
    public function __construct(protected Serializer $serializer)
    {
    }


    /**
     * @throws ValidateException
     */
    public function validateRequiredParams(Request $request, array $getParams): void
    {
        $validators = [];
        foreach ($getParams as $param) {
            $validators[$param] = ValGr::m($request->get($param), Req::m());
        }
        ValidateManager::validateOrThrow($validators);
    }

    /**
     * @throws ValidateException
     */
    public function validateStringParams(Request $request, array $getParams): void
    {
        $validators = [];
        foreach ($getParams as $param) {
            $validators[$param] = ValGr::m($request->get($param), Str::m()->nullable()->mustEmpty());
        }
        ValidateManager::validateOrThrow($validators);
    }

    /**
     * @throws ValidateException
     */
    public function validateRequiredStringParams(Request $request, array $getParams): void
    {
        $validators = [];
        foreach ($getParams as $param) {
            $validators[$param] = ValGr::m($request->get($param), [Req::m(), Str::m()]);
        }
        ValidateManager::validateOrThrow($validators);
    }

    public function validateErrorResponse(array $errors, array $paramKeys = [], string|array $payloadKey = 'payload'): JsonResponse
    {
        $commonErrors = [];
        $paramErrors = [];
        $payloadErrors = [];
        $undeclaredErrors = [];

        foreach ($errors as $key => $error) {
            if ($key === '') {
                $commonErrors[] = $error;
                continue;
            }

            if (is_string($payloadKey)) {
                if ($key === $payloadKey) {
                    $payloadErrors = $error;
                    continue;
                }
            } else {
                if (in_array($key, $payloadKey)) {
                    $payloadErrors[$key] = $error;
                    continue;
                }
            }

            if (in_array($key, $paramKeys)) {
                $paramErrors[$key] = $error;
                continue;
            }

            $undeclaredErrors[$key] = $error;
        }

        if (count($undeclaredErrors) > 0) {
            $implodedErrors = [];
            foreach ($undeclaredErrors as $key => $error) {
                $stringError = is_string($error) ? $error : json_encode($error, JSON_UNESCAPED_UNICODE);
                $implodedErrors[] = "`$key`: $stringError";
            }
            throw new RuntimeException("undeclared errors: " . implode(', ', $implodedErrors));
        }

        $responseArray = [];

        if (count($commonErrors) > 0) {
            $responseArray['commonError'] = $commonErrors;
        }
        if (count($paramErrors) > 0) {
            $responseArray['paramErrors'] = $paramErrors;
        }
        if (count($payloadErrors) > 0) {
            $responseArray['payloadErrors'] = $payloadErrors;
        }

        return response()->json($responseArray)->setStatusCode(422);
    }

    public function notFound(string $message = ''): JsonResponse
    {
        return response()->json($message ? ['message' => $message] : [])->setStatusCode(404);
    }
}

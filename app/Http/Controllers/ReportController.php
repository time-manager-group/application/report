<?php

namespace App\Http\Controllers;

use App\Services\MetricService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Symfony\Component\HttpFoundation\Response;

class ReportController extends Controller
{
    public function getList(Request $request): Response
    {
        $all = $request->all();

        $count = DB::table('reports')->count();
        $page = (int)Arr::get($all, 'page', 1) ?: 1;
        $perPage = (int)Arr::get($all, 'per_page', $count);

        $oderBy = (string)Arr::get($all, 'oder_by', 'created_at');

        if (!in_array($oderBy, [
            'id',
            'name',
            'created_at',
            'updated_at',
        ], true)) {
            $oderBy = 'created_at';
        }

        $oderDirection = (string)Arr::get($all, 'oder_direction', 'asc');

        if (!in_array($oderDirection, [
            'asc',
            'desc',
        ], true)) {
            $oderDirection = 'asc';
        }

        $query = DB::table('reports')
            ->addSelect('id')
            ->addSelect('name')
            ->orderBy($oderBy, $oderDirection);

        if ($page && $perPage) {
            $offset = $perPage * ($page - 1);
            $limit = $perPage;

            $query
                ->offset($offset)
                ->limit($limit);
        }

        $reports = $query->get()
            ->toArray();

        return $this->serializer->serialize([
            'list' => $reports,
            'pagination' => [
                'count' => $count,
                'page' => $page,
                'per_page' => $perPage,
            ],
        ]);
    }

    public function create(Request $request): Response
    {
        $all = $request->all();

        $name = Arr::get($all, 'name');

        $newReportId = DB::table('reports')
            ->insertGetId([
                'name' => $name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

        Cache::forever('lastLoadedReportId', $newReportId);

        return $this->serializer->serialize(['id' => $newReportId]);
    }

    public function getLast(): Response
    {
        $lastLoadedReportId = Cache::get('lastLoadedReportId');

        if ($lastLoadedReportId
            && DB::table('reports')
                ->addSelect('id')
                ->where('id', $lastLoadedReportId)
                ->exists()) {
            return $this->serializer->serialize(['id' => $lastLoadedReportId]);
        }

        $lastReport = DB::table('reports')
            ->addSelect('id')
            ->orderBy('id', 'desc')
            ->first();

        if ($lastReport) {
            Cache::forever('lastLoadedReportId', $lastReport->id);
            return $this->serializer->serialize(['id' => $lastReport->id]);
        }

        return $this->serializer->serialize(['id' => null]);
    }

    public function getActive(): Response
    {
        $activeReport = DB::table('reports')
            ->addSelect('reports.id')
            ->leftJoin('tracks', 'reports.id', 'tracks.report_id')
            ->where('tracks.is_active', true)
            ->orderBy('tracks.created_at', 'desc')
            ->limit(1)
            ->get()
            ->toArray();

        return $this->serializer->serialize(['id' => $activeReport ? $activeReport[0]->id : null]);
    }

    public function getNameForCreate(): Response
    {
        $name = Carbon::now()->format('Y.m.d');
        $origin = $name;

        $reports = DB::table('reports')
            ->addSelect('name')
            ->where('name', 'like', $name . '%')
            ->get()
            ->toArray();

        $names = array_column($reports, 'name');

        $count = 2;

        while (in_array($name, $names, true)) {
            $name = "$origin ($count)";
            $count++;
        }

        return $this->serializer->serialize(['name' => $name]);
    }

    public function getMetrics(MetricService $metricService): Response
    {
        $keys = [
            'cron:sendDailyReport:before',
            'cron:sendDailyReport:after',
            'http:sendDailyReport:before',
            'http:sendDailyReport:after',
            'cron:everyMinute',
        ];

        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $metricService->getTime($key);
        }

        return response()->json(['data' => $result]);
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use JsonException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class ExportImportController extends Controller
{
    /**
     * @throws JsonException
     */
    public function export(int $reportId): Response
    {
        $reports = DB::table('reports')
            ->addSelect('name')
            ->addSelect('created_at')
            ->addSelect('updated_at')
            ->where('id', $reportId)
            ->limit(1)
            ->get();
        
        if (!count($reports)) {
            throw new NotFoundHttpException("Report with id $reportId not found");
        }
        $tracks = DB::table('tracks')
            ->addSelect('tracks.id')
            ->addSelect('tracks.name')
            ->addSelect('tracks.created_at')
            ->addSelect('tracks.updated_at')
            ->addSelect('tracks.date_start')
            ->addSelect('tracks.date_end')
            ->addSelect('tracks.is_active')
            ->addSelect('track_types.code')
            ->leftJoin('track_types', 'tracks.type_id', 'track_types.id')
            ->orderBy('tracks.id')
            ->where('tracks.report_id', $reportId)
            ->get();
        
        $result = [];
        
        foreach ($tracks as $track) {
            $result[] = [
                'name' => $track->name,
                'createdAt' => $track->created_at,
                'updatedAt' => $track->updated_at,
                'dateStart' => $track->date_start,
                'dateEnd' => $track->date_end,
                'isActive' => (bool)$track->is_active,
                'type' => $track->code,
            ];
        }
        
        $report = $reports[0];
        
        $export = [
            'report' => [
                'name' => $report->name,
                'createdAt' => $report->created_at,
                'updatedAt' => $report->updated_at,
            ],
            'tracks' => $result,
        ];
        
        $fileName = 'report-' . $report->name . '-' . Carbon::now()->format('Y_m_d-H_i_s') . '.json';
        
        $content = json_encode($export, JSON_THROW_ON_ERROR + JSON_PRETTY_PRINT);
        
        $response = new Response();
        $response->setContent($content);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $fileName . '"');
        
        return $response;
    }
    
    
    /**
     * @throws Exception
     */
    public function import(Request $request): Response
    {
        $reportIdForUpdate = (int)$request->get('reportIdForUpdate');
        
        if ($reportIdForUpdate
            && !DB::table('reports')
                ->where('id', $reportIdForUpdate)->exists()) {
            throw new NotFoundHttpException("Report with id $reportIdForUpdate not found");
        }
        
        $file = $request->file('reportFile');
        
        if ($file === null) {
            throw new BadRequestHttpException('field `reportFile` required and must be binary file!');
        }
        
        $content = file_get_contents($file->getPathname());
        
        try {
            $importData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            throw new BadRequestHttpException('Invalid json in file');
        }
        
        DB::beginTransaction();
        
        try {
            $reportId = $reportIdForUpdate ?: $this->createReport($importData['report']);
            $this->createTracks($importData['tracks'], $reportId);
            
            DB::commit();
        } catch (Throwable $t) {
            DB::rollback();
            return response()->json(['error' => $t->getMessage()], 500);
        }
        
        return $this->serializer->serialize(['id' => $reportId]);
    }
    
    private function createReport(array $report): int
    {
        $newReportId = DB::table('reports')
            ->insertGetId([
                'name' => $report['name'],
                'created_at' => Carbon::parse($report['createdAt']),
                'updated_at' => Carbon::parse($report['updatedAt']),
            ]);
        
        Cache::forever('lastLoadedReportId', $newReportId);
        
        return $newReportId;
    }
    
    /**
     * @throws Exception
     */
    private function createTracks(array $tracks, int $reportId): void
    {
        $typeItemList = DB::table('track_types')
            ->select(['id', 'code'])
            ->get()
            ->pluck('id', 'code')
            ->toArray();
        
        foreach ($tracks as $track) {
            $name = Arr::get($track, 'name');
            $type = Arr::get($track, 'type');
            $createdAt = Arr::get($track, 'createdAt');
            $updatedAt = Arr::get($track, 'updatedAt');
            $dateStart = Arr::get($track, 'dateStart');
            $dateEnd = Arr::get($track, 'dateEnd');
            $isActive = Arr::get($track, 'isActive');
            
            $this->checkDuplicateActive($reportId, $isActive);
            
            $typeItemId = $typeItemList[$type];
            
            $newTrackId = DB::table('tracks')
                ->insertGetId([
                    'name' => $name,
                    'created_at' => Carbon::parse($createdAt),
                    'updated_at' => $updatedAt ? Carbon::parse($updatedAt) : null,
                    'date_start' => Carbon::parse($dateStart),
                    'date_end' => $dateEnd ? Carbon::parse($dateEnd) : null,
                    'type_id' => $typeItemId,
                    'is_active' => (bool)$isActive,
                    'report_id' => $reportId,
                ]);
            
            if (!$newTrackId) {
                throw new Exception("no affected track import `$name`");
            }
        }
    }
    
    /**
     * @throws Exception
     */
    private function checkDuplicateActive(int $reportId, bool $isActive): void
    {
        if (!$isActive) {
            return;
        }
        
        $query = DB::table('tracks')
            ->addSelect('id')
            ->where('report_id', $reportId)
            ->where('is_active', true);
        
        $count = $query->get()
            ->count();
        
        if ($count > 0) {
            throw new Exception('conflict: active track must be one');
        }
    }
}

<?php

namespace App\Exceptions;

use Exception;

class ValidateException extends Exception
{
    /**
     * @var array<string,string|array>
     */
    private array $errors;

    public function __construct(array $errors)
    {
        parent::__construct('Validation error', 400);
        $this->errors = $errors;
    }

    /**
     * @return array<string,string|array>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}

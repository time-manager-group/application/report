<?php

namespace App\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    /**
     * @throws NotFoundException
     */
    public static function throw(string $message = ''): void
    {
        throw new self($message);
    }

    /**
     * @throws NotFoundException
     */
    public static function throwIfNull($value, string $message = ''): void
    {
        if (is_null($value)) {
            self::throw($message);
        }
    }
}

<?php

namespace App\Services;

use App\Entities\KVMetric;
use App\Repositories\MetricKVRepository;
use App\Tools\EntityHelper;
use Carbon\Carbon;

class MetricService
{
    private const K_NOW_TIME = 'now_time';

    public function __construct(private MetricKVRepository $repository)
    {
    }

    public function setNowTime(string $key): void
    {
        $key = [$key, self::K_NOW_TIME];

        $payload = [
            'key' => $key,
            'value' => [
                'ts' => Carbon::now()->getTimestamp(),
            ],
        ];

        if ($this->repository->has($key)) {
            $entity = EntityHelper::fromArr($payload + [
                    'updatedAt' => Carbon::now()->getTimestamp(),
                ], $this->repository->get($key));
        } else {
            $entity = EntityHelper::fromArr($payload + [
                    'createdAt' => Carbon::now()->getTimestamp(),
                ], KVMetric::class);
        }
        $this->repository->set($entity);
    }

    public function getTime(string $key): int|null
    {
        $key = [$key, self::K_NOW_TIME];
        if ($this->repository->has($key)) {
            return $this->repository->get($key)->value['ts'];
        }

        return null;
    }
}

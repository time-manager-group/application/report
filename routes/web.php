<?php

use App\Http\Controllers\ExportImportController;
use App\Http\Controllers\HealthCheckController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TrackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name("Проверка\nдоступности сервиса")
    ->get('/healthcheck', [HealthCheckController::class, 'ping']);

/**
 * Отчеты.
 */
Route::name('Список отчетов')
    ->get('', [ReportController::class, 'getList']);

Route::name("Оптимальное имя\nдля нового\nотчета")
    ->get('/nameForCreate', [ReportController::class, 'getNameForCreate']);

Route::name("Создание\nнового отчета")
    ->post('', [ReportController::class, 'create']);

Route::name('Отчет последний')
    ->get('/last', [ReportController::class, 'getLast']);

Route::name('Отчет активный')
    ->get('/active', [ReportController::class, 'getActive']);

Route::get('/metrics', [ReportController::class, 'getMetrics']);

/**
 * Треки отчета.
 */
Route::name("Список треков\nотчета")
    ->get('/{reportId}/track', [TrackController::class, 'getList']);

Route::name("Создание нового\nтрека в отчете")
    ->post('/{reportId}/track', [TrackController::class, 'create']);

Route::name("Обновить трек\nв отчете")
    ->put('/{reportId}/track/{trackId}', [TrackController::class, 'update']);

Route::name("Удалить трек\nиз отчета")
    ->delete('/{reportId}/track/{trackId}', [TrackController::class, 'delete']);

/**
 * Выгрузки и загрузки.
 */
Route::name('Выгрузить отчет')
    ->get('/export/{reportId}', [ExportImportController::class, 'export']);
Route::name('Импортировать отчет')
    ->post('/import', [ExportImportController::class, 'import']);
